# ImageBrowse

**本项目是基于开源项目ImageBrowse进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/brucetoo/ImageBrowse ）追踪到原项目版本**

#### 项目介绍
* 项目名称：ImageBrowse
* 所属系列：ohos的第三方组件适配移植
* 功能：图片预览。
* 项目移植状态：完成
* 调用差异：无
* 项目作者和维护人：hihope
* 联系方式：hihope@hoperun.com
* 原项目Doc地址：https://github.com/brucetoo/ImageBrowse
* 编程语言：Java
* 外部库依赖：无
* 原项目基线版本：无release版本 , sha1:3ce7c8b9db297308a064365b4042026d37e09e63

#### 演示效果

<img src="gif/demo.gif"/>

#### 安装教程

方法1.

1. 下载依赖ImageBrowse的har包：ImageBrowse.har。

2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

   ```
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
   	……
   }
   ```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'com.brucetoo.imagebrowse.ohos:imagebrowse:1.0.0'
}
```

#### 使用说明
``` java
     PhotoView p = new PhotoView(getContext());
     Display density = DisplayManager.getInstance().getDefaultDisplay(getContext()).get();
    
     p.setLayoutConfig(new TableLayout.LayoutConfig((int) (density.getAttributes().scalDensity * 100), (int) (density.getAttributes().scalDensity * 100)));
     p.setScaleMode(Image.ScaleMode.CLIP_CENTER);
    
     p.setEnabled(false);
```
```
     PacMap bundle = new PacMap();
     bundle.putStringList(ImageInfo.INTENT_IMAGE_URLS, imgList);
     bundle.putSequenceableObject(ImageInfo.INTENT_CLICK_IMAGE_INFO, ((PhotoView) component).getInfo());
     bundle.putIntValue(ImageInfo.INTENT_CLICK_IMAGE_POSITION, position);
     imgImageInfos.clear();
     //NOTE:if imgList.size >= the visible count in single screen,i will cause NullPointException
     //because item out of screen have been replaced/reused
     for (int i = 0; i < imgList.size(); i++) {
         imgImageInfos.add(((PhotoView) listContainer.getComponentAt(i)).getInfo());
     }
     listContainer.getComponentAt(position); //putSequenceableObjectList
     bundle.putSequenceableObjectList(ImageInfo.INTENT_IMAGE_INFOS, imgImageInfos);

     getFractionManager().startFractionScheduler()
             .replace(ResourceTable.Id_layout_root, ImageBrowseFragment.newInstance(bundle))
             .submit();
```
#### 版本迭代
* 1.0.0

  实现功能

  1. 下载的图片预览。

#### 版权和许可信息
 ```
  Copyright 2015-2016 Bruce too
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
      http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 ```