package com.brucetoo.imagebrowse;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

/**
 * Created by Bruce Too
 * On 9/26/15.
 * At 15:13
 * ActivityTransitionExitHelper和 ActivityTransitionEnterHelper
 * 实现了Activity直接切换的时候自定义view动画
 */
public class ActivityTransitionEnterHelper {

    public final static String PRE_NAME = "ActivityTransitionEnterHelper";
    private final Ability activity;
    private Component fromView;//the view where u click
    private String imgUrl;// the resource url of imageView etc..

    public ActivityTransitionEnterHelper(Ability activity) {
        this.activity = activity;
    }

    public static ActivityTransitionEnterHelper with(Ability activity) {
        return new ActivityTransitionEnterHelper(activity);
    }

    public ActivityTransitionEnterHelper fromView(Component fromView) {
        this.fromView = fromView;
        return this;
    }

    public ActivityTransitionEnterHelper imageUrl(String imgUrl) {
        this.imgUrl = imgUrl;
        return this;
    }

    public void start(Class target) {
        int[] screenLocation = new int[2];
        screenLocation = fromView.getLocationOnScreen();

        Intent intent = new Intent();
        intent.setParam(PRE_NAME + ".left", screenLocation[0])
                .setParam(PRE_NAME + ".top", screenLocation[1])
                .setParam(PRE_NAME + ".width", fromView.getEstimatedWidth())
                .setParam(PRE_NAME + ".height", fromView.getEstimatedHeight())
                .setParam(PRE_NAME + ".imageUrl", imgUrl);
        activity.startAbility(intent);
        activity.getContext().setTransitionAnimation(0,0);
    }

}
