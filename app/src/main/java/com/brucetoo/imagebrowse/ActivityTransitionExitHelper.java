package com.brucetoo.imagebrowse;

import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorScatter;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentTreeObserver;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;


/**
 * Created by Bruce Too
 * On 9/26/15.
 * At 15:13
 * ActivityTransitionExit
 * Don`t forget add transparent theme in target activity
 * <style name="Transparent">
 * <item name="ohos:windowNoTitle">true</item>
 * <item name="ohos:windowIsTranslucent">true</item>
 * <item name="ohos:windowBackground">$colorL:transparent</item>
 * </style>
 */
public class ActivityTransitionExitHelper {


    private static final int ANIM_DURATION = 500;
    private Intent fromIntent;//intent from pre activity
    private Component toView;//target view show in this activity
    private Component background; //root view of this activity
    private ShapeElement bgDrawable; //background color
    private int leftDelta;
    private int topDelta;
    private float widthDelta;
    private float heightDelta;

    public ActivityTransitionExitHelper(Intent fromIntent) {
        this.fromIntent = fromIntent;
    }

    public static ActivityTransitionExitHelper with(Intent intent) {
        return new ActivityTransitionExitHelper(intent);
    }

    /**
     * add target view
     *
     * @param toView
     * @return
     */
    public ActivityTransitionExitHelper toView(Component toView) {
        this.toView = toView;
        return this;
    }

    /**
     * add root view of this layout
     *
     * @param background
     * @return
     */
    public ActivityTransitionExitHelper background(Component background) {
        this.background = background;
        return this;
    }

    /**
     * @param savedInstanceState if savedInstanceState != null
     *                           we don`t have to perform the transition animation
     */
    public ActivityTransitionExitHelper start(Intent savedInstanceState) {
        if (savedInstanceState == null) {
            final int thumbnailTop = fromIntent.getIntParam(ActivityTransitionEnterHelper.PRE_NAME + ".top", 0);
            final int thumbnailLeft = fromIntent.getIntParam(ActivityTransitionEnterHelper.PRE_NAME + ".left", 0);
            final int thumbnailWidth = fromIntent.getIntParam(ActivityTransitionEnterHelper.PRE_NAME + ".width", 0);
            final int thumbnailHeight = fromIntent.getIntParam(ActivityTransitionEnterHelper.PRE_NAME + ".height", 0);
            String imgUrl = fromIntent.getStringParam(ActivityTransitionEnterHelper.PRE_NAME + ".imageUrl");

            bgDrawable = new ShapeElement();
            bgDrawable.setRgbColor(RgbColor.fromArgbInt(Color.BLACK.getValue()));
            background.setBackground(bgDrawable);
            toView.getComponentTreeObserver().addWindowBoundListener(new ComponentTreeObserver.WindowBoundListener() {
                @Override
                public void onWindowBound() {
                    int viewLocation[] = new int[2];
                    viewLocation = toView.getLocationOnScreen();
                    leftDelta = thumbnailLeft - viewLocation[0];
                    topDelta = thumbnailTop - viewLocation[1];
                    //Note: widthDelta must be float
                    widthDelta = (float) thumbnailWidth / toView.getWidth();
                    heightDelta = (float) thumbnailHeight / toView.getHeight();

                    runEnterAnimation();
                }

                @Override
                public void onWindowUnbound() {

                }
            });
        }
        return this;
    }

    private void runEnterAnimation() {
        //resize/relocation toView
        toView.setPivotX(0);
        toView.setPivotY(0); //axis
        toView.setScaleX(widthDelta);
        toView.setScaleY(heightDelta);
        toView.setTranslationX(leftDelta);
        toView.setTranslationY(topDelta);

        AnimatorValue animate = new AnimatorValue();
        animate.setDuration(ANIM_DURATION);
        animate.setCurveType(Animator.CurveType.LINEAR);
        animate.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float value) {
                float [] position =toView.getContentPosition();
                float scaleX =position[0]+1;
                float scaleY = position[1]+1;
                toView.setContentPosition((scaleX*value),(scaleY*value));
            }
        });
        animate.start();
    }

    public void runExitAnimation(final Runnable exit) {

        AnimatorProperty animatorProperty =  new AnimatorProperty();
        animatorProperty.setTarget(toView).moveToX(widthDelta).moveToY(heightDelta)
                .setDuration(ANIM_DURATION)
                .alphaFrom(1).alpha(0)
                .setStateChangedListener(new Animator.StateChangedListener() {
                    @Override
                    public void onStart(Animator animator) {

                    }

                    @Override
                    public void onStop(Animator animator) {

                    }

                    @Override
                    public void onCancel(Animator animator) {

                    }

                    @Override
                    public void onEnd(Animator animator) {
                        background.setVisibility(Component.HIDE);
                        toView.setVisibility(Component.HIDE); //let background and target view invisible
                        exit.run();
                    }

                    @Override
                    public void onPause(Animator animator) {

                    }

                    @Override
                    public void onResume(Animator animator) {

                    }
                }).start();
    }
}
