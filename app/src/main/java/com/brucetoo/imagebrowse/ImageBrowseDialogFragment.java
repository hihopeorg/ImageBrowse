package com.brucetoo.imagebrowse;

import com.brucetoo.imagebrowse.widget.ImageInfo;
import com.brucetoo.imagebrowse.widget.MaterialProgressBar;
import com.brucetoo.imagebrowse.widget.PhotoView;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.multimodalinput.event.KeyEvent;
import ohos.utils.PacMap;
import ohos.utils.Sequenceable;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

/**
 * Created by Bruce Too
 * On 6/12/16.
 * At 10:10
 * ImageBrowseDialogFragment
 * FullScreen or not has different situation... Status Bar height
 */
public class ImageBrowseDialogFragment extends Fraction {

    private PageSlider viewPager;
    private Text tips; //viewpager indicator
    private ArrayList<String> imageUrls;
    private ImageInfo imageInfo;
    private Component mask;//background view
    private ArrayList<Sequenceable> imageInfos;//ImageInfo
    private int position;
    private boolean isExit = false;
    private Component RootComponent;
    private static PacMap mPacMap = null;

    public static ImageBrowseDialogFragment newInstance(PacMap imgs) {
        ImageBrowseDialogFragment fragment = new ImageBrowseDialogFragment();
        mPacMap = imgs;
        return fragment;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);

        viewPager = (PageSlider) RootComponent.findComponentById(ResourceTable.Id_viewpager);
        tips = (Text) RootComponent.findComponentById(ResourceTable.Id_text);
        mask = RootComponent.findComponentById(ResourceTable.Id_mask);

        runEnterAnimation();

        imageUrls = mPacMap.getStringList(ImageInfo.INTENT_IMAGE_URLS);
        imageInfo = (ImageInfo) mPacMap.getObjectValue(ImageInfo.INTENT_CLICK_IMAGE_INFO).get();
        imageInfos = mPacMap.getSequenceableList(ImageInfo.INTENT_IMAGE_INFOS);
        position = mPacMap.getIntValue(ImageInfo.INTENT_CLICK_IMAGE_POSITION, 0);

        tips.setText((position + 1) + "/" + imageUrls.size());
        viewPager.setProvider(new PageSliderProvider() {
            int Count = 0;
            @Override
            public int getCount() {
                return imageUrls.size();
            }

            @Override
            public Object createPageInContainer(ComponentContainer componentContainer, int pos) {
                Component view = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_layout_view_detail, null, false);
                final PhotoView photoView = (PhotoView) view.findComponentById(ResourceTable.Id_image_detail);
                final MaterialProgressBar progressBar = (MaterialProgressBar) view.findComponentById(ResourceTable.Id_progress);

                String imgUrl = imageUrls.get(pos);
                Count = pos;

                if (Utils.isImageCacheAvailable(imgUrl)) {//full image cache is available

                    Utils.displayImageWithCache(imgUrl, photoView, new RequestListener<Element>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Element> target, boolean b) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Element element, Object o, Target<Element> target, DataSource dataSource, boolean b) {
                            photoView.setImageDrawable(element);
                            progressBar.setVisibility(Component.HIDE);
                            progressBar.stopSpinning();
                            return true;
                        }
                    });
                }

                //force to get focal point,to listen key listener
                photoView.setTouchFocusable(true);
                photoView.setClickedListener(onClickListener);
                photoView.setTag(pos);
                photoView.enableGesture();

                componentContainer.addComponent(view);

                return view;
            }

            @Override
            public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
                componentContainer.removeComponent((Component) o);
            }

            @Override
            public boolean isPageMatchToObject(Component component, Object o) {
                return false;
            }
        });
        viewPager.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {

            }

            @Override
            public void onPageSlideStateChanged(int i) {
                tips.setText((position + 1) + "/" + imageUrls.size());
            }

            @Override
            public void onPageChosen(int i) {

            }
        });

        viewPager.setCurrentPage(position);
    }


    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        return RootComponent = scatter.parse(ResourceTable.Layout_fragment_viewpager, null, false);
    }

    private Component.ClickedListener onClickListener = new Component.ClickedListener() {
        @Override
        public void onClick(Component v) {
            exitFragment(v);
        }
    };

    private void exitFragment(Component v) {
        //退出时点击的位置
        int position = (int) v.getTag();
        //回到上个界面该view的位置
        if (((StackLayout) v.getComponentParent()).getComponentAt(1).getVisibility() == Component.VISIBLE) {
            onStop();
        } else {
            runExitAnimation(v);
            ((PhotoView) v).animaTo((ImageInfo) imageInfos.get(position), new Runnable() {
                @Override
                public void run() {
                    onStop();
                }
            });
        }
    }

    private void runEnterAnimation() {
        AnimatorProperty animatorProperty = new AnimatorProperty();
        animatorProperty.setTarget(mask);
        animatorProperty.setDuration(300);
        animatorProperty.start();
    }

    public void runExitAnimation(final Component view) {
        AnimatorProperty animatorProperty = new AnimatorProperty();
        animatorProperty.alphaFrom(1).alpha(0)
                .setStateChangedListener(new Animator.StateChangedListener() {
                    @Override
                    public void onStart(Animator animator) {

                    }

                    @Override
                    public void onStop(Animator animator) {

                    }

                    @Override
                    public void onCancel(Animator animator) {

                    }

                    @Override
                    public void onEnd(Animator animator) {
                        mask.setVisibility(Component.HIDE);
                        view.setVisibility(Component.HIDE);
                    }

                    @Override
                    public void onPause(Animator animator) {

                    }

                    @Override
                    public void onResume(Animator animator) {

                    }
                });
        animatorProperty.start();
    }

}
