package com.brucetoo.imagebrowse;

import com.brucetoo.imagebrowse.widget.ImageInfo;
import com.brucetoo.imagebrowse.widget.MaterialProgressBar;
import com.brucetoo.imagebrowse.widget.PhotoView;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.utils.PacMap;
import ohos.utils.Sequenceable;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

/**
 * Created by Bruce Too
 * On 9/28/15.
 * At 19:37
 * ImageBrowseFragment add into MainActivity
 */
public class ImageBrowseFragment extends Fraction {

    private PageSlider viewPager;
    private Text tips; //viewpager indicator
    private ArrayList<String> imageUrls;
    private ImageInfo imageInfo;
    private Component mask;//background view
    private ArrayList<Sequenceable> imageInfos;
    private int position;
    public static Component RootBrowser;
    private static PacMap mPacMap = null;
    public static PhotoView photoview;

    public static ImageBrowseFragment newInstance(PacMap imgs) {
        ImageBrowseFragment fragment = new ImageBrowseFragment();
        mPacMap = imgs;
        return fragment;
    }


    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        RootBrowser = scatter.parse(ResourceTable.Layout_fragment_viewpager, container, false);
        RootBrowser.setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT));

        viewPager = (PageSlider) RootBrowser.findComponentById(ResourceTable.Id_viewpager);
        tips = (Text) RootBrowser.findComponentById(ResourceTable.Id_text);
        mask = RootBrowser.findComponentById(ResourceTable.Id_mask);

        runEnterAnimation();
        PacMap bundle = mPacMap;
        imageUrls = bundle.getStringList(ImageInfo.INTENT_IMAGE_URLS);
        imageInfo = (ImageInfo) bundle.getObjectValue(ImageInfo.INTENT_CLICK_IMAGE_INFO).get();
        imageInfos = bundle.getSequenceableList(ImageInfo.INTENT_IMAGE_INFOS);
        position = bundle.getIntValue(ImageInfo.INTENT_CLICK_IMAGE_POSITION, 0);
        tips.setText((position + 1) + "/" + imageUrls.size());

        viewPager.setProvider(new PageSliderProvider() {
            int Count = 0;

            @Override
            public int getCount() {
                return imageUrls.size();
            }

            @Override
            public Object createPageInContainer(ComponentContainer componentContainer, int pos) {
                Component view = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_layout_view_detail, null, false);
                final PhotoView photoView = (PhotoView) view.findComponentById(ResourceTable.Id_image_detail);
                final MaterialProgressBar progressBar = (MaterialProgressBar) view.findComponentById(ResourceTable.Id_progress);
                photoView.setScaleMode(Image.ScaleMode.CENTER);//setScaleMode
                photoview = photoView;

                String imgUrl = imageUrls.get(pos);
                Count = pos;

                if (Utils.isImageCacheAvailable(imgUrl)) {//full image cache is available

                    Utils.displayImageWithCache(imgUrl, photoView, new RequestListener<Element>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Element> target, boolean b) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Element element, Object o, Target<Element> target, DataSource dataSource, boolean b) {
                            photoView.setImageDrawable(element);
                            progressBar.setVisibility(Component.HIDE);
                            progressBar.stopSpinning();
                            return false;
                        }
                    });

                }
                //force to get focal point,to listen key listener
                photoView.setTouchFocusable(true);
                photoView.setClickedListener(onClickListener);
                photoView.setTag(pos);
                photoView.enableGesture();

                componentContainer.addComponent(view);
                return view;
            }

            @Override
            public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
                componentContainer.removeComponent((Component) o);
            }

            @Override
            public boolean isPageMatchToObject(Component component, Object object) {
                return component == object;
            }
        });

        viewPager.addPageChangedListener(new PageSlider.PageChangedListener() {
            int pageNumber = 0;

            @Override
            public void onPageSliding(int i, float v, int i1) {
            }

            @Override
            public void onPageSlideStateChanged(int i) {
            }

            @Override
            public void onPageChosen(int i) {
                tips.setText((i + 1) + "/" + imageUrls.size());
                pageNumber = i;
            }
        });
        viewPager.setCurrentPage(position);
        return RootBrowser;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }

    private Component.ClickedListener onClickListener = new Component.ClickedListener() {
        @Override
        public void onClick(Component v) {
            exitFragment(v);
        }
    };

    private void exitFragment(Component v) {
        int position = (int) v.getTag();
        if (((StackLayout) v.getComponentParent()).getComponentAt(0).getVisibility() == Component.HIDE) {
            popFragment();
        } else {
            runExitAnimation(mask,v);
            ((PhotoView) v).animaTo((ImageInfo) imageInfos.get(position), new Runnable() {
                @Override
                public void run() {
                    popFragment();
                }
            });
        }
    }


    private void popFragment() {
        onInactive();
        final FractionAbility fragmentManager = getFractionAbility();
        if (fragmentManager != null) {
            fragmentManager.getFractionManager().popFromStack();
        }
    }


    private void runEnterAnimation() {
        AnimatorProperty animatorProperty = new AnimatorProperty();
        animatorProperty.setDuration(300);
        animatorProperty.setTarget(mask);
        animatorProperty.alphaFrom(0);
        animatorProperty.alpha(1f);
        animatorProperty.start();
    }

    public void runExitAnimation(final Component mask,Component view) {
        AnimatorProperty animatorProperty = new AnimatorProperty();
        animatorProperty.alphaFrom(1);
        animatorProperty.alpha(0);
        animatorProperty.setTarget(mask);
        animatorProperty.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                mask.setVisibility(Component.HIDE);
                viewPager.setVisibility(Component.HIDE);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        animatorProperty.start();

        AnimatorProperty animatorPropertyView = new AnimatorProperty();
        animatorPropertyView.alphaFrom(1);
        animatorPropertyView.alpha(0);
        animatorPropertyView.setTarget(view);
        animatorPropertyView.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {

            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
                view.setVisibility(Component.HIDE);
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        animatorPropertyView.start();

    }

}
