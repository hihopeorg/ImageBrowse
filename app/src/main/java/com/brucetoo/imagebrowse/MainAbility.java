/*
 * Copyright (C) 2013 The ohos Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.brucetoo.imagebrowse;

import com.brucetoo.imagebrowse.widget.ImageInfo;
import com.brucetoo.imagebrowse.widget.PhotoView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.bundle.IBundleManager;
import ohos.utils.PacMap;
import ohos.utils.Sequenceable;
import ohos.utils.net.Uri;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

public class MainAbility extends FractionAbility {
    private ListContainer gridView;
    private ArrayList<String> imgList = new ArrayList<>();
    private ArrayList<Sequenceable> imgImageInfos = new ArrayList<>();
    private final static int mRequestCode = 1;
    private boolean initView = false;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_activity_main);

        initView();
    }

    private void initView() {
        initView = true;
        imgList.add(0, "http://img6.cache.netease.com/3g/2015/9/30/20150930091938133ad.jpg");
        imgList.add(1, "http://img2.cache.netease.com/3g/2015/9/30/2015093000515435aff.jpg");
        imgList.add(2, "http://img5.cache.netease.com/3g/2015/9/30/20150930075225737e5.jpg");
        imgList.add(3, "http://img5.cache.netease.com/3g/2015/9/29/20150929213007cd8cd.jpg");
        imgList.add(4, "http://img3.cache.netease.com/3g/2015/9/29/20150929162747a8bfa.jpg");
        imgList.add(5, "http://img2.cache.netease.com/3g/2015/9/30/20150930091208cf03c.jpg");
        imgList.add(6, "http://img2.cache.netease.com/3g/2015/9/30/2015093000515435aff.jpg");
        imgList.add(7, "http://img5.cache.netease.com/3g/2015/9/29/20150929213007cd8cd.jpg");
        imgList.add(8, "http://img3.cache.netease.com/3g/2015/9/29/20150929162747a8bfa.jpg");
        gridView = (ListContainer) findComponentById(ResourceTable.Id_gridview);

        TableLayoutManager tableLayoutManager = new TableLayoutManager();
        tableLayoutManager.setColumnCount(3);
        gridView.setLayoutManager(tableLayoutManager);
        final MainAbility.ImageAdapter adapter = new MainAbility.ImageAdapter();

        gridView.setItemProvider(adapter);

        gridView.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int position, long l) {
                if (component.isEnabled()) {
                    //Use of ImageBrowseFragment
                    PacMap bundle = new PacMap();
                    bundle.putStringList(ImageInfo.INTENT_IMAGE_URLS, imgList);
                    bundle.putSequenceableObject(ImageInfo.INTENT_CLICK_IMAGE_INFO, ((PhotoView) component).getInfo());
                    bundle.putIntValue(ImageInfo.INTENT_CLICK_IMAGE_POSITION, position);
                    imgImageInfos.clear();
                    //NOTE:if imgList.size >= the visible count in single screen,i will cause NullPointException
                    //because item out of screen have been replaced/reused
                    for (int i = 0; i < imgList.size(); i++) {
                        imgImageInfos.add(((PhotoView) listContainer.getComponentAt(i)).getInfo());
                    }
                    listContainer.getComponentAt(position); //putSequenceableObjectList
                    bundle.putSequenceableObjectList(ImageInfo.INTENT_IMAGE_INFOS, imgImageInfos);

                    getFractionManager().startFractionScheduler()
                            .replace(ResourceTable.Id_layout_root, ImageBrowseFragment.newInstance(bundle))
                            .submit();

                }
            }
        });

    }


    class ImageAdapter extends BaseItemProvider {

        @Override
        public int getCount() {
            return imgList.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(RgbColor.fromRgbaInt(Color.getIntColor("#9e9e9e")));
            componentContainer.setBackground(shapeElement);
            PhotoView p = new PhotoView(getContext());
            Display density = DisplayManager.getInstance().getDefaultDisplay(getContext()).get();

            p.setLayoutConfig(new TableLayout.LayoutConfig((int) (density.getAttributes().scalDensity * 100), (int) (density.getAttributes().scalDensity * 100)));
            p.setScaleMode(Image.ScaleMode.CLIP_CENTER);
            p.disable();//enable touch


            Glide.with(getContext()).load(Uri.parse("https://img-pre.ivsky.com/img/tupian/pre/202103/15/sunyunzhu_changxiushan-010.jpg")).listener(new RequestListener<Element>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Element> target, boolean b) {
                    return false;
                }

                @Override
                public boolean onResourceReady(Element element, Object o, Target<Element> target, DataSource dataSource, boolean b) {
                    p.setEnabled(true);

                    p.setImageDrawable(element);
                    return b;
                }
            }).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(p);
            return p;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        initView = false;
    }
}
