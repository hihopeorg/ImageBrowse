/*
 * Copyright (C) 2013 The ohos Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.brucetoo.imagebrowse;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.HslColor;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;

/**
 * display PictureDetailsActivity
 */
public class PictureDetailsActivity extends Ability {

    private static final int ANIM_DURATION = 500;

    ShapeElement mBackground;
    int mLeftDelta;
    int mTopDelta;
    float mWidthScale;
    float mHeightScale;
    private Image mImageView;
    private DependentLayout mBackgroudnView;
    private ActivityTransitionExitHelper transitionExitHelper;

    private Text mTextView;


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_picture_info);
        mImageView = (Image) findComponentById(ResourceTable.Id_imageView);
        mBackgroudnView = (DependentLayout) findComponentById(ResourceTable.Id_topLevelLayout);

        transitionExitHelper = ActivityTransitionExitHelper.with(getIntent())
                .toView(mImageView).background(mBackgroudnView).start(intent);

    }

    @Override
    public void onBackPressed() {
        terminateAbility();
        transitionExitHelper.runExitAnimation(new Runnable() {
            @Override
            public void run() {
                onStop();
            }
        });
    }

    @Override
    public void terminateAbility(int requestCode) {
        super.terminateAbility(requestCode);
    }

    @Override
    protected void onStop() {
        super.onStop();
        setTransitionAnimation(0,0);
    }

    /**
     * The enter animation scales the picture in from its previous imageUrl
     * size/location, colorizing it in parallel. In parallel, the background of the
     * activity is fading in. When the pictue is in place, the text description
     * drops down.
     */
    public void runEnterAnimation() {
        final long duration = (long) (ANIM_DURATION);

        // Set starting values for properties we're going to animate. These
        // values scale and position the full size version down to the imageUrl
        // size/location, from which we'll animate it back up
        mImageView.setPivotX(0);
        mImageView.setPivotY(0);
        mImageView.setScaleX(mWidthScale);
        mImageView.setScaleY(mHeightScale);
        mImageView.setTranslationX(mLeftDelta);
        mImageView.setTranslationY(mTopDelta);

        // We'll fade the text in later
        mTextView.setAlpha(0);

        // Animate scale and translation to go from imageUrl to full size
        AnimatorProperty animatorProperty = new AnimatorProperty();
        animatorProperty.setTarget(mImageView);
        animatorProperty.scaleX(1).scaleY(1)
                .setCurveType(Animator.CurveType.SMOOTH_STEP)
                .setStateChangedListener(new Animator.StateChangedListener() {
                    @Override
                    public void onStart(Animator animator) {

                    }

                    @Override
                    public void onStop(Animator animator) {

                    }

                    @Override
                    public void onCancel(Animator animator) {

                    }

                    @Override
                    public void onEnd(Animator animator) {
                        mTextView.setTranslationY(-mTextView.getHeight());
                        mTextView.createAnimatorProperty()
                                .setDuration(duration / 2)
                                .alpha(1);
                    }

                    @Override
                    public void onPause(Animator animator) {

                    }

                    @Override
                    public void onResume(Animator animator) {

                    }
                });

        // Fade in the black background
        this.getCurrentFocus().createAnimatorProperty().alphaFrom(0).alpha(100).setDelay(duration).start();
        AnimatorValue bgAnim = new AnimatorValue();
        bgAnim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                mBackground.setAlpha((int) v * 100);
            }
        });
        bgAnim.start();

        // Animate a color filter to take the image from grayscale to full color.
        // This happens in parallel with the image scaling and moving into place.

        AnimatorValue colorizer = new AnimatorValue();
        colorizer.setDelay(duration);
        colorizer.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                ShapeElement shapeElement = (ShapeElement) PictureDetailsActivity.this.getWindow().getCurrentComponentFocus().get().getBackgroundElement();
                RgbColor[] rgbColors = shapeElement.getRgbColors();
                float[] hslfloat = Utils.rgbToHsl(rgbColors[0].getRed(), rgbColors[0].getGreen(), rgbColors[0].getBlue());
                HslColor hslColor = new HslColor(hslfloat[0], hslfloat[1], hslfloat[2]);
                hslColor.setSaturation(v);
                int[] rgbcolor = Utils.hslToRgb(hslColor.getHue(), hslColor.getSaturation(), hslColor.getLightness());
                RgbColor rgbColor = new RgbColor(rgbcolor[0], rgbcolor[1], rgbcolor[2]);
                PictureDetailsActivity.this.getWindow().setBackgroundColor(rgbColor);
            }
        });
        colorizer.start();

    }
}
