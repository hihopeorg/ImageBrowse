package com.brucetoo.imagebrowse;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.global.resource.ResourceManager;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import org.jetbrains.annotations.Nullable;

import static ohos.agp.render.opengl.Utils.EPSILON;

/**
 * Created by Bruce Too
 * On 6/13/16.
 * At 22:47
 */
public class Utils {

    /**
     * get a thumbnail image url from original url
     *
     * @param imgUrl original image url
     * @param width  width u need
     * @param height height u need
     * @return the number(85) in below url indicate the quality of original image
     */
    public static String getThumbnailImageUrl(ResourceManager resource, String imgUrl, int width, int height) {
        String url = "https://img-pre.ivsky.com/img/tupian/pre/202103/15/sunyunzhu_changxiushan-007.jpg";
        return url;
    }

    public static boolean isImageCacheAvailable(String url){
       return true;
    }

    public static void displayImageWithCache(String url, Image imageView,RequestListener<Element> listener){
        Glide.with(imageView.getContext()).load(url)
                .addListener(listener)
                .into(imageView);
    }

    public static float[] rgbToHsl(int pR, int pG, int pB) {

        float r = pR / 255f;

        float g = pG / 255f;

        float b = pB / 255f;



        float max = (r > g && r > b) ? r : (g > b) ? g : b;

        float min = (r < g && r < b) ? r : (g < b) ? g : b;



        float h, s, l;

        l = (max + min) / 2.0f;



        if (compareFloat(max , min)) {

            h = s = 0.0f;

        } else {

            float d = max - min;

            s = (l > 0.5f) ? d / (2.0f - max - min) : d / (max + min);



            if (r > g && r > b)

                h = (g - b) / d + (g < b ? 6.0f : 0.0f);



            else if (g > b)

                h = (b - r) / d + 2.0f;



            else

                h = (r - g) / d + 4.0f;



            h /= 6.0f;

        }



        float[] hsl = {h, s, l};

        return hsl;

    }

    public static int[] hslToRgb(float h, float s, float l){

        float r, g, b;



        if (s == 0f) {

            r = g = b = l; // achromatic

        } else {

            float q = l < 0.5f ? l * (1 + s) : l + s - l * s;

            float p = 2 * l - q;

            r = hueToRgb(p, q, h + 1f/3f);

            g = hueToRgb(p, q, h);

            b = hueToRgb(p, q, h - 1f/3f);

        }

        int[] rgb = {(int) (r * 255), (int) (g * 255), (int) (b * 255)};

        return rgb;

    }

    public static float hueToRgb(float p, float q, float t) {

        if (t < 0f)

            t += 1f;

        if (t > 1f)

            t -= 1f;

        if (t < 1f/6f)

            return p + (q - p) * 6f * t;

        if (t < 1f/2f)

            return q;

        if (t < 2f/3f)

            return p + (q - p) * (2f/3f - t) * 6f;

        return p;

    }

    public static boolean compareFloat(float a, float b) {
        return (Math.abs(a - b)) < EPSILON;
    }

    public static PixelMap createPixelMap(int width, int height) {
        PixelMap pixelMap;
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(width, height);
        options.pixelFormat = PixelFormat.ARGB_8888;
        options.editable = true;
        pixelMap = PixelMap.create(options);
        return pixelMap;

    }
}
