/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.brucetoo.imagebrowse.compat;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.Optional;

public class DisplayUtils {

    public static Element getTransparentElement(){
        ShapeElement element = new ShapeElement();
        element.setAlpha(0);
        return element;
    }

    public static Element getColorElement(Color color){
        ShapeElement element = new ShapeElement();
        element.setAlpha(255);
        element.setRgbColor(RgbColor.fromArgbInt(color.getValue()));
        return element;
    }

    public static Element getColorElement(int color){
        ShapeElement element = new ShapeElement();
        element.setAlpha(255);
        element.setRgbColor(RgbColor.fromArgbInt(color));
        return element;
    }

    /**
     * 获取屏幕高度，不包含状态栏
     * 返回：
     * height = point.getPointY
     * width = point.getPointX
     *
     * @param context
     * @return size
     */
    public static Point getDisplaySize(Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        Point point = new Point();
        display.getSize(point);
        return point;
    }

    /**
     * 获取屏幕密度
     * @param context
     * @return
     */
    public static float getDensity(Context context) {
        Optional<Display> defaultDisplay = DisplayManager.getInstance().getDefaultDisplay(context);
        return defaultDisplay.map(display -> display.getRealAttributes().densityPixels).orElse(1f);
    }

    public static int vp2Px(Context context, float dpValue) {
        return (int) (dpValue * getDensity(context) + 0.5f);
    }

    public static int px2vp(Context context, int pxValue){
        Optional<Display> defaultDisplay = DisplayManager.getInstance().getDefaultDisplay(context);
        int densityDpi = defaultDisplay.map(display -> display.getRealAttributes().densityDpi).orElse(1);
        return densityDpi * pxValue;
    }
}
