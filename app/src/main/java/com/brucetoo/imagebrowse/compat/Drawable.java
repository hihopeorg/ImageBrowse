/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.brucetoo.imagebrowse.compat;

import ohos.agp.components.Image;
import ohos.app.Context;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.File;

public class Drawable {
    private PixelMap pixelMap;
    private Context context;

    private Drawable(Context ctx, PixelMap pixelMap) {
        this.context = ctx;
        this.pixelMap = pixelMap;
    }

    public static Drawable create(Context context, int resId) {
        return new Drawable(context, getResImage(context, resId));
    }

    public static Drawable create(Image imgView) {
        return new Drawable(imgView.getContext(), imgView.getPixelMap());
    }

    public static Drawable create(Context context, File imgFile) {
        try {
            ImageSource imageSource = ImageSource.create(imgFile, null);
            PixelMap pixelMap = createPixelMap(imageSource);
            return new Drawable(context, pixelMap);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 通过资源ID获取位图对象
     **/
    public static PixelMap getResImage(Context context, int resId) {
        try (Resource resource = context.getResourceManager().getResource(resId)) {
            ImageSource imageSource = ImageSource.create(resource, null);
            return createPixelMap(imageSource);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static PixelMap createPixelMap(ImageSource imageSource) throws Exception {
        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
        return imageSource.createPixelmap(decodingOptions);
    }


    public PixelMap getPixelMap() {
        return pixelMap;
    }

    public int getWidth() {
        if (pixelMap == null || pixelMap.getImageInfo() == null) {
            return 0;
        }
        return pixelMap.getImageInfo().size.width;
    }

    public int getHeight() {
        if (pixelMap == null || pixelMap.getImageInfo() == null)
            return 0;
        else
            return pixelMap.getImageInfo().size.height;
    }

    public int getIntrinsicWidth() {
        return DisplayUtils.px2vp(context, getWidth());
    }

    public int getIntrinsicHeight() {
        return DisplayUtils.px2vp(context, getHeight());
    }
}
