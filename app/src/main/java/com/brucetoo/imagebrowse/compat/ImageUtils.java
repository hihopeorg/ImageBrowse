/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.brucetoo.imagebrowse.compat;

import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ImageUtils {

    public static PixelMap createPixelMap(File file) {
        return null;
    }


    public static List<PixelMap> createPixelMapGif(File file) {
        List<PixelMap> mapList = new ArrayList<>();
        ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
        sourceOptions.formatHint = "image/gif";
        ImageSource imageSource = ImageSource.create(file, sourceOptions);
        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
        decodingOptions.allowPartialImage = true;
        if (imageSource != null) {
            int index = 0;
            PixelMap map;
            do {
                try {
                    map = imageSource.createPixelmap(index, decodingOptions);
                    if (map == null) {
                        break;
                    }
                    mapList.add(map);
                    index++;
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            } while (map != null);
        }

        return mapList;
    }


    /**
     * For GIF, we only need 3 bytes, 'GIF',
     * For WebP, we need 12 bytes, 'RIFF' + size + 'WEBP',
     * to determine still/animated WebP, we need 5 extra bytes, 4 bytes chunk header to check
     * for extended WebP format, 1 byte to check for animated bit.
     * <p>
     * reference: https://developers.google.com/speed/webp/docs/riff_container
     */
    public static FileType getImageType(File file) {
        FileType type = FileType.TYPE_STILL_IMAGE;
        try (FileInputStream inputStream = new FileInputStream(file)) {
            final int ANIMATED_WEBP_MASK = 0x02;
            byte[] header = new byte[20];
            int read = inputStream.read(header);
            if (read >= 3 && isGifHeader(header)) {
                type = FileType.TYPE_GIF;
            } else if (read >= 12 && isWebpHeader(header)) {
                if (read >= 17 && isExtendedWebp(header)
                        && (header[16] & ANIMATED_WEBP_MASK) != 0) {
                    type = FileType.TYPE_ANIMATED_WEBP;
                } else {
                    type = FileType.TYPE_STILL_WEBP;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return type;
    }


    private static boolean isGifHeader(byte[] header) {
        return header[0] == 'G' && header[1] == 'I' && header[2] == 'F';
    }

    private static boolean isWebpHeader(byte[] header) {
        return header[0] == 'R' && header[1] == 'I' && header[2] == 'F' && header[3] == 'F'
                && header[8] == 'W' && header[9] == 'E' && header[10] == 'B' && header[11] == 'P';
    }

    private static boolean isExtendedWebp(byte[] header) {
        return header[12] == 'V' && header[13] == 'P' && header[14] == '8' && header[15] == 'X';
    }

    public enum FileType {
        TYPE_GIF, TYPE_STILL_WEBP, TYPE_ANIMATED_WEBP, TYPE_STILL_IMAGE
    }

}
