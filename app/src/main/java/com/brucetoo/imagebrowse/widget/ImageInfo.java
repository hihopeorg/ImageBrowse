package com.brucetoo.imagebrowse.widget;

import ohos.agp.components.Image;
import ohos.agp.utils.RectFloat;
import ohos.interwork.utils.ParcelableEx;
import ohos.utils.PacMap;
import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

/**
 * Created by Bruce too
 * On 2016/4/18
 * At 20:26
 */
public class ImageInfo implements Sequenceable {

    public static final String INTENT_IMAGE_URLS = "imageUrls";
    public static final String INTENT_CLICK_IMAGE_POSITION = "preImagePosition";
    public static final String INTENT_CLICK_IMAGE_INFO = "clickImageInfo";
    public static final String INTENT_IMAGE_INFOS = "imageInfos";

    // 内部图片在整个窗口的位置
    RectFloat mRect = new RectFloat();
    // 控件在窗口的位置
    RectFloat mLocalRect = new RectFloat();
    RectFloat mImgRect = new RectFloat();
    RectFloat mWidgetRect = new RectFloat();
    float mScale;
    float mDegrees;
    Image.ScaleMode mScaleType;

    public ImageInfo(RectFloat rect, RectFloat local, RectFloat img, RectFloat widget, float scale, float degrees, Image.ScaleMode scaleType) {
        mRect.modify(rect);
        mLocalRect.modify(local);
        mImgRect.modify(img);
        mWidgetRect.modify(widget);
        mScale = scale;
        mScaleType = scaleType;
        mDegrees = degrees;
    }

    protected ImageInfo(Parcel in) {
        mRect = (RectFloat) in.readParcelableEx(RectFloat.class.getClassLoader());
        mLocalRect = (RectFloat) in.readParcelableEx(RectFloat.class.getClassLoader());
        mImgRect = (RectFloat) in.readParcelableEx(RectFloat.class.getClassLoader());
        mWidgetRect = (RectFloat) in.readParcelableEx(RectFloat.class.getClassLoader());
        mScale = in.readFloat();
        mDegrees = in.readFloat();
    }

    /**
     * Correct ImageInfo Use in {@link com.brucetoo.imagebrowse.ImageBrowseDialogFragment}
     *
     * @param rootLocation    root view's location in screen
     * @param statusBarHeight use status bar's height if {@link com.brucetoo.imagebrowse.ImageBrowseDialogFragment#onActivityCreated(PacMap)}
     *                        no set fragment fullscreen flag...
     */
    public void correct(int[] rootLocation, int statusBarHeight) {
        mRect.left = mRect.left + rootLocation[0];
        mRect.right = mRect.right + rootLocation[0];
        mRect.top = mRect.top + rootLocation[1] - statusBarHeight;
        mRect.bottom = mRect.bottom + rootLocation[1] - statusBarHeight;

        mLocalRect.left = mLocalRect.left + rootLocation[0];
        mLocalRect.right = mLocalRect.right + rootLocation[0];
        mLocalRect.top = mLocalRect.top + rootLocation[1] - statusBarHeight;
        mLocalRect.bottom = mLocalRect.bottom + rootLocation[1] - statusBarHeight;

        mImgRect.left = mImgRect.left + rootLocation[0];
        mImgRect.right = mImgRect.right + rootLocation[0];
        mImgRect.top = mImgRect.top + rootLocation[1] - statusBarHeight;
        mImgRect.bottom = mImgRect.bottom + rootLocation[1] - statusBarHeight;

        mWidgetRect.left = mWidgetRect.left + rootLocation[0];
        mWidgetRect.right = mWidgetRect.right + rootLocation[0];
        mWidgetRect.top = mWidgetRect.top + rootLocation[1] - statusBarHeight;
        mWidgetRect.bottom = mWidgetRect.bottom + rootLocation[1] - statusBarHeight;
    }

    public static final Producer CREATOR = new Producer() {
        @Override
        public Object createFromParcel(Parcel parcel) {
            return new ImageInfo(parcel);
        }
    };


    @Override
    public boolean marshalling(Parcel parcel) {
        parcel.writeParcelableEx((ParcelableEx) mRect);
        parcel.writeParcelableEx((ParcelableEx) mLocalRect);
        parcel.writeParcelableEx((ParcelableEx) mImgRect);
        parcel.writeParcelableEx((ParcelableEx) mWidgetRect);
        parcel.writeFloat(mScale);
        parcel.writeFloat(mDegrees);
        return true;
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        return false;
    }
}
