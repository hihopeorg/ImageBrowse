package com.brucetoo.imagebrowse.widget;

import com.brucetoo.imagebrowse.Utils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Paint.Style;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.miscservices.timeutility.Time;
import ohos.os.ProcessManager;
import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

import static com.brucetoo.imagebrowse.Utils.compareFloat;

/**
 * Material Progress
 * by Bruce too
 * 使用方法：
 * 1.不确定进度的使用
 * 只需要设置view 就Ok  另外可设置 颜色setBarColor.宽度setBarWidth.速度等等...
 * 2.确定进度的使用
 * setInstantProgress()初始化进度条，会自动停止 spin()的拉伸动画
 * setProgerss()设置进度   setCallback()设置进度更新回调
 */
public class MaterialProgressBar extends Component implements Component.EstimateSizeListener, Component.ComponentStateChangedListener, Component.DrawTask {
    private static final String TAG = MaterialProgressBar.class.getSimpleName();

    /**
     * *********
     * DEFAULTS *
     * **********
     */
    //Sizes (with defaults in DP)
    private int circleRadius = 28; //默认圆半径
    private int barWidth = 5;  //边界宽度 拉伸效过的边界
    private int rimWidth = 2;  //边界宽度 封闭区域的边界

    private final int barLength = 16; //边界最小长度
    private final int barMaxLength = 270; //边界最大

    private boolean fillRadius = false; // 画圆是否填充

    private double timeStartGrowing = 0;
    private double barSpinCycleTime = 460;
    private float barExtraLength = 0;
    private boolean barGrowingFromFront = true;

    private long pausedTimeWithoutGrowing = 0;
    private final long pauseGrowingTime = 200;

    //Colors (with defaults)
    private int barColor = Color.getIntColor("#cfcfcf");
    private int rimColor = 0x00FFFFFF;

    //Paints
    private Paint barPaint = null;
    private Paint rimPaint = null;

    //Rectangles
    private RectFloat circleBounds = new RectFloat();

    //Animation
    //The amount of degrees per second
    private float spinSpeed = 230.0f;
    //private float spinSpeed = 120.0f;
    // The last time the spinner was animated
    private long lastTimeAnimated = 0;

    private boolean linearProgress;

    private float mProgress = 0.0f;
    private float mTargetProgress = 0.0f;
    private boolean isSpinning = false;

    private ProgressCallback callback;

    private boolean isfrist = false;


    public MaterialProgressBar(Context context) {
        super(context);
        parseAttributes();
        setEstimateSizeListener(this::onEstimateSize);
        setComponentStateChangedListener(this::onComponentStateChanged);
        setupPaints();
        addDrawTask(this::onDraw);

    }

    /**
     * The constructor for the ProgressWheel
     *
     * @param context
     * @param attrSet
     */
    public MaterialProgressBar(Context context, AttrSet attrSet) {
        super(context, attrSet);
        parseAttributes();
        setEstimateSizeListener(this::onEstimateSize);
        setComponentStateChangedListener(this::onComponentStateChanged);
        setupPaints();
        addDrawTask(this::onDraw);
    }

    public MaterialProgressBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        parseAttributes();
        setEstimateSizeListener(this::onEstimateSize);

        setComponentStateChangedListener(this::onComponentStateChanged);
        setupPaints();

        addDrawTask(this::onDraw);

    }


    //----------------------------------
    //Setting up stuff
    //----------------------------------
    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int viewWidth = circleRadius + this.getPaddingLeft() + this.getPaddingRight();
        int viewHeight = circleRadius + this.getPaddingTop() + this.getPaddingBottom();
        int widthMode = EstimateSpec.getMode(widthMeasureSpec);
        int widthSize = EstimateSpec.getSize(widthMeasureSpec);
        int heightMode = EstimateSpec.getMode(heightMeasureSpec);
        int heightSize = EstimateSpec.getSize(heightMeasureSpec);

        int widthConfig =0;
        int heightConfig = 0;

        //Measure Width
        if (widthMode == EstimateSpec.PRECISE) {
            //Must be this size
            widthConfig = widthSize;
        } else if (widthMode == EstimateSpec.NOT_EXCEED) {
            //Can't be bigger than...
            widthConfig = Math.min(viewWidth, widthSize);
        } else {
            //Be whatever you want
            widthConfig = viewWidth;
        }

        //Measure Height
        if (heightMode == EstimateSpec.PRECISE || widthMode == EstimateSpec.PRECISE) {
            //Must be this size
            heightConfig = heightSize;
        } else if (heightMode == EstimateSpec.NOT_EXCEED) {
            //Can't be bigger than...
            heightConfig = Math.min(viewHeight, heightSize);
        } else {
            //Be whatever you want
            heightConfig = viewHeight;
        }
        setupBounds(widthConfig, heightConfig);
        setEstimatedSize(240, 240);
        return false;
    }
    /**
     * Set the properties of the paints we're using to
     * draw the progress wheel
     */
    private void setupPaints() {
        barPaint = new Paint();
        rimPaint = new Paint();
        barPaint.setColor(new Color(barColor));
        barPaint.setAntiAlias(true);
        barPaint.setStyle(Style.STROKE_STYLE);
        barPaint.setStrokeWidth(barWidth);

        rimPaint.setColor(new Color(rimColor));
        rimPaint.setAntiAlias(true);
        rimPaint.setStyle(Style.STROKE_STYLE);
        rimPaint.setStrokeWidth(rimWidth);
    }

    /**
     * Set the bounds of the component
     */
    private void setupBounds(int layout_width, int layout_height) {
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();

        if (!fillRadius) {
            // Width should equal to Height, find the min value to setup the circle
            int minValue = Math.min(layout_width - paddingLeft - paddingRight,
                    layout_height - paddingBottom - paddingTop);
            int circleDiameter = Math.min(minValue, circleRadius * 2 - barWidth * 2);

            // Calc the Offset if needed for centering the wheel in the available space
            int xOffset = (layout_width - paddingLeft - paddingRight - circleDiameter) / 2 + paddingLeft;
            int yOffset = (layout_height - paddingTop - paddingBottom - circleDiameter) / 2 + paddingTop;

            circleBounds = new RectFloat(xOffset + barWidth,
                    yOffset + barWidth,
                    xOffset + circleDiameter - barWidth,
                    yOffset + circleDiameter - barWidth);
        } else {
            circleBounds = new RectFloat(paddingLeft + barWidth,
                    paddingTop + barWidth,
                    layout_width - paddingRight - barWidth,
                    layout_height - paddingBottom - barWidth);
        }
    }

    /**
     * Parse the attributes passed to the view from the XML
     */
    private void parseAttributes() {
        // We transform the default values from DIP to pixels
        barWidth = vp2px(getContext(), barWidth);
        rimWidth = vp2px(getContext(), rimWidth);
        circleRadius = vp2px(getContext(), circleRadius);
        fillRadius = false;

        float baseSpinSpeed = spinSpeed / 360.0f;
        spinSpeed = baseSpinSpeed * 360;

        //barSpinCycleTime = barSpinCycleTime;

//        barColor = a.getColor(R.styleable.ProgressWheel_matProg_barColor, barColor);
//
//        rimColor = a.getColor(R.styleable.ProgressWheel_matProg_rimColor, rimColor);

        linearProgress = false;

        //  if (a.getBoolean(R.styleable.ProgressWheel_matProg_progressIndeterminate, false)) {
        spin();
        //  }

        // Recycle
//        a.recycle();
    }

    public void setCallback(ProgressCallback progressCallback) {
        callback = progressCallback;

        if (!isSpinning) {
            runCallback();
        }
    }

    //----------------------------------
    //Animation stuff
    //----------------------------------
    @Override
    public void onDraw(Component component, Canvas canvas) {
        Arc arc = new Arc();
        arc.setArc(360, 360, false);
        canvas.drawArc(circleBounds, arc, rimPaint);

        boolean mustInvalidate = false;

        if (isSpinning) {
            //Draw the spinning bar
            mustInvalidate = true;
            long deltaTime = (Time.getRealActiveTime() - lastTimeAnimated);
            float deltaNormalized = deltaTime * spinSpeed / 1000.0f;

            updateBarLength(deltaTime);

            mProgress += deltaNormalized;
            if (mProgress > 360) {
                mProgress -= 360f;

                // A full turn has been completed
                // we run the callback with -1 in case we want to
                // do something, like changing the color
                runCallback(-1.0f);
            }
            lastTimeAnimated = Time.getRealActiveTime();

            float from = mProgress - 90;
            float length = barLength + barExtraLength;

            canvas.drawArc(circleBounds, new Arc(from, length, false), barPaint);
        } else {
            float oldProgress = mProgress;

            if (mProgress != mTargetProgress) {
                //We smoothly increase the progress bar
                mustInvalidate = true;

                float deltaTime = (float) (Time.getRealActiveTime() - lastTimeAnimated) / 1000;
                float deltaNormalized = deltaTime * spinSpeed;

                mProgress = Math.min(mProgress + deltaNormalized, mTargetProgress);
                lastTimeAnimated = Time.getRealActiveTime();
            }

            if (oldProgress != mProgress) {
                runCallback();
            }

            float offset = 0.0f;
            float progress = mProgress;
            if (!linearProgress) {
                float factor = 2.0f;
                offset = (float) (1.0f - Math.pow(1.0f - mProgress / 360.0f, 2.0f * factor)) * 360.0f;
                progress = (float) (1.0f - Math.pow(1.0f - mProgress / 360.0f, factor)) * 360.0f;
            }

            canvas.drawArc(circleBounds, new Arc(offset - 90, progress, false), barPaint);
        }

        if (mustInvalidate) {
            component.getContext().getUITaskDispatcher().delayDispatch(new Runnable() {
                @Override
                public void run() {
                    invalidate();
                }
            }, 50);
        }
    }


    @Override
    public void onComponentStateChanged(Component component, int visibility) {
        if (visibility == VISIBLE) {
            lastTimeAnimated = Time.getRealActiveTime();
        }
    }

    private void updateBarLength(long deltaTimeInMilliSeconds) {
        if (pausedTimeWithoutGrowing >= pauseGrowingTime) {
            timeStartGrowing += deltaTimeInMilliSeconds;

            if (timeStartGrowing > barSpinCycleTime) {
                // We completed a size change cycle
                // (growing or shrinking)
                timeStartGrowing -= barSpinCycleTime;
                //if(barGrowingFromFront) {
                pausedTimeWithoutGrowing = 0;
                //}
                barGrowingFromFront = !barGrowingFromFront;
            }

            float distance = (float) Math.cos((timeStartGrowing / barSpinCycleTime + 1) * Math.PI) / 2 + 0.5f;
            float destLength = (barMaxLength - barLength);

            if (barGrowingFromFront) {
                barExtraLength = distance * destLength;
            } else {
                float newLength = destLength * (1 - distance);
                mProgress += (barExtraLength - newLength);
                barExtraLength = newLength;
            }
        } else {
            pausedTimeWithoutGrowing += deltaTimeInMilliSeconds;
        }
    }

    /**
     * Check if the wheel is currently spinning
     */

    public boolean isSpinning() {
        return isSpinning;
    }

    /**
     * Reset the count (in increment mode)
     */
    public void resetCount() {
        mProgress = 0.0f;
        mTargetProgress = 0.0f;
        invalidate();
    }

    /**
     * Turn off spin mode
     */
    public void stopSpinning() {
        isSpinning = false;
        mProgress = 0.0f;
        mTargetProgress = 0.0f;
        invalidate();
    }

    /**
     * Puts the view on spin mode
     */
    public void spin() {
        lastTimeAnimated = Time.getRealActiveTime();
        isSpinning = true;
        invalidate();
    }

    private void runCallback(float value) {
        if (callback != null) {
            callback.onProgressUpdate(value);
        }
    }

    private void runCallback() {
        if (callback != null) {
            float normalizedProgress = (float) Math.round(mProgress * 100 / 360.0f) / 100;
            callback.onProgressUpdate(normalizedProgress);
        }
    }

    /**
     * Set the progress to a specific value,
     * the bar will smoothly animate until that value
     *
     * @param progress the progress between 0 and 1
     */
    public void setProgress(float progress) {
        if (isSpinning) {
            mProgress = 0.0f;
            isSpinning = false;

            runCallback();
        }

        if (progress > 1.0f) {
            progress -= 1.0f;
        } else if (progress < 0) {
            progress = 0;
        }

        if (compareFloat(progress , mTargetProgress)) {
            return;
        }

        // If we are currently in the right position
        // we set again the last time animated so the
        // animation starts smooth from here
        if (compareFloat(mProgress , mTargetProgress)) {
            lastTimeAnimated = Time.getRealActiveTime();
        }

        mTargetProgress = Math.min(progress * 360.0f, 360.0f);

        invalidate();
    }

    /**
     * Set the progress to a specific value,
     * the bar will be set instantly to that value
     *
     * @param progress the progress between 0 and 1
     */
    public void setInstantProgress(float progress) {
        if (isSpinning) {
            mProgress = 0.0f;
            isSpinning = false;
        }

        if (progress > 1.0f) {
            progress -= 1.0f;
        } else if (progress < 0) {
            progress = 0;
        }

        if (compareFloat(progress, mTargetProgress)) {
            return;
        }

        mTargetProgress = Math.min(progress * 360.0f, 360.0f);
        mProgress = mTargetProgress;
        lastTimeAnimated = Time.getRealActiveTime();
        invalidate();
    }

    //----------------------------------
    //Getters + setters
    //----------------------------------

    /**
     * @return the current progress between 0.0 and 1.0,
     * if the wheel is indeterminate, then the result is -1
     */
    public float getProgress() {
        return isSpinning ? -1 : mProgress / 360.0f;
    }

    /**
     * Sets the determinate progress mode
     *
     * @param isLinear if the progress should increase linearly
     */
    public void setLinearProgress(boolean isLinear) {
        linearProgress = isLinear;
        if (!isSpinning) {
            invalidate();
        }
    }

    /**
     * @return the radius of the wheel in pixels
     */
    public int getCircleRadius() {
        return circleRadius;
    }

    /**
     * Sets the radius of the wheel
     *
     * @param circleRadius the expected radius, in pixels
     */
    public void setCircleRadius(int circleRadius) {
        this.circleRadius = circleRadius;
        if (!isSpinning) {
            invalidate();
        }
    }

    /**
     * @return the width of the spinning bar
     */
    public int getBarWidth() {
        return barWidth;
    }

    /**
     * Sets the width of the spinning bar
     *
     * @param barWidth the spinning bar width in pixels
     */
    public void setBarWidth(int barWidth) {
        this.barWidth = barWidth;
        if (!isSpinning) {
            invalidate();
        }
    }

    /**
     * @return the color of the spinning bar
     */
    public int getBarColor() {
        return barColor;
    }

    /**
     * Sets the color of the spinning bar
     *
     * @param barColor The spinning bar color
     */
    public void setBarColor(int barColor) {
        this.barColor = barColor;
        setupPaints();
        if (!isSpinning) {
            invalidate();
        }
    }

    /**
     * @return the color of the wheel's contour
     */
    public int getRimColor() {
        return rimColor;
    }

    /**
     * Sets the color of the wheel's contour
     *
     * @param rimColor the color for the wheel
     */
    public void setRimColor(int rimColor) {
        this.rimColor = rimColor;
        setupPaints();
        if (!isSpinning) {
            invalidate();
        }
    }

    /**
     * @return the base spinning speed, in full circle turns per second
     * (1.0 equals on full turn in one second), this value also is applied for
     * the smoothness when setting a progress
     */
    public float getSpinSpeed() {
        return spinSpeed / 360.0f;
    }

    /**
     * Sets the base spinning speed, in full circle turns per second
     * (1.0 equals on full turn in one second), this value also is applied for
     * the smoothness when setting a progress
     *
     * @param spinSpeed the desired base speed in full turns per second
     */
    public void setSpinSpeed(float spinSpeed) {
        this.spinSpeed = spinSpeed * 360.0f;
    }

    /**
     * @return the width of the wheel's contour in pixels
     */
    public int getRimWidth() {
        return rimWidth;
    }

    /**
     * Sets the width of the wheel's contour
     *
     * @param rimWidth the width in pixels
     */
    public void setRimWidth(int rimWidth) {
        this.rimWidth = rimWidth;
        if (!isSpinning) {
            invalidate();
        }
    }


    /**
     * 更新progress的回调方法
     */
    public interface ProgressCallback {
        /**
         * Method to call when the progress reaches a value
         * in order to avoid float precision issues, the progress
         * is rounded to a float with two decimals.
         * <p>
         * In indeterminate mode, the callback is called each time
         * the wheel completes an animation cycle, with, the progress value is -1.0f
         *
         * @param progress a double value between 0.00 and 1.00 both included
         */
        public void onProgressUpdate(float progress);
    }

    public static int vp2px(Context context, float vp) {
        DisplayAttributes attributes = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
        return (int) (attributes.densityPixels * vp);
    }
}
