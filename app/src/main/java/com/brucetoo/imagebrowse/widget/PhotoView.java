package com.brucetoo.imagebrowse.widget;

import com.brucetoo.imagebrowse.compat.DisplayUtils;
import com.brucetoo.imagebrowse.compat.Drawable;
import com.brucetoo.imagebrowse.compat.GestureDetector;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.math.BigDecimal;
import java.util.Optional;


/**
 * Created by Bruce too
 * On 2016/4/18
 * At 20:26
 * code from https://github.com/bm-x/PhotoView
 */

public class PhotoView extends Image implements Component.DrawTask, Component.DoubleClickedListener {
    public final static String TAG = "PhotoView";
    public final static int ANIMATE_DURING = 300;

    private final static int MIN_ROTATE = 35;
    private final static int ANIMA_DURING = 340;
    private final static float MAX_SCALE = 2.5f;

    private int mMinRotate;
    private int mAnimaDuring;
    private float mMaxScale;

    private int MAX_OVER_SCROLL = 0;
    private int MAX_FLING_OVER_SCROLL = 0;
    private int MAX_OVER_RESISTANCE = 0;
    private int MAX_ANIM_FROM_WAITE = 500;

    private Matrix mBaseMatrix = new Matrix();
    private Matrix mAnimaMatrix = new Matrix();
    private Matrix mSynthesisMatrix = new Matrix();
    private Matrix mTmpMatrix = new Matrix();

    private RotateGestureDetector mRotateDetector;
    private GestureDetector mDetector;
    private Component.ClickedListener mClickListener;

    private Image.ScaleMode mScaleType;

    private boolean hasMultiTouch;
    private boolean hasDrawable;
    private boolean isKnowSize;
    private boolean hasOverTranslate;
    private boolean isEnableGesture = true;
    private boolean isInit;
    private boolean mAdjustViewBounds;
    // 当前是否处于放大状态
    private boolean isZoomUp;
    private boolean canRotate;
    private boolean forceFinished;

    private boolean imgLargeWidth;
    private boolean imgLargeHeight;

    private float mRotateFlag;
    private float mDegrees;
    private float mScale = 1.0f;
    private int mTranslateX;
    private int mTranslateY;

    private float mHalfBaseRectWidth;
    private float mHalfBaseRectHeight;

    private RectFloat mWidgetRect = new RectFloat();
    private RectFloat mBaseRect = new RectFloat();
    private RectFloat mImgRect = new RectFloat();
    private RectFloat mTmpRect = new RectFloat();
    private RectFloat mCommonRect = new RectFloat();

    private Point mScreenCenter = new Point();
    private Point mScaleCenter = new Point();
    private Point mRotateCenter = new Point();

    private RectFloat mClip;
    private ImageInfo mFromInfo;
    private long mInfoTime;
    private Runnable mCompleteCallBack;
    private PhotoView mPhotoView;
    private static final float SCALE_MIN = 1.0f;
    private boolean isDouble = false;

    private Component.LongClickedListener mLongClick;

    public PhotoView(Context context) {
        super(context);
        init();
    }

    public PhotoView(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public PhotoView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        super.setScaleMode(ScaleMode.ZOOM_CENTER);
        if (mScaleType == null) mScaleType = ScaleMode.INSIDE;
        mRotateDetector = new RotateGestureDetector(mRotateListener);
        mDetector = new GestureDetector(getContext(), mGestureListener);
        this.setTouchEventListener(this::onTouchEvent);
        setDoubleClickedListener(this);
        setTouchFocusable(true);

        Optional<Display> defaultDisplay = DisplayManager.getInstance().getDefaultDisplay(getContext());
        float density = defaultDisplay.map(display -> display.getRealAttributes().densityPixels).orElse(1F);
        MAX_OVER_SCROLL = (int) (density * 30);
        MAX_FLING_OVER_SCROLL = (int) (density * 30);
        MAX_OVER_RESISTANCE = (int) (density * 140);

        mMinRotate = MIN_ROTATE;
        mAnimaDuring = ANIMA_DURING;
        mMaxScale = MAX_SCALE;
        addDrawTask(this);
        mPhotoView = this;
    }

    Point pointFirst, pointLastMove;
    long lastMoveTime;
    float startPosition;
    float downX1 = 0, downX2 = 0, downY1 = 0, downY2 = 0, oldDist = 0;
    double moveDist;

    private boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (isEnableGesture) {
            mDetector.onTouchEvent(touchEvent);
            mRotateDetector.onTouchEvent(touchEvent);
            int pointerCount = touchEvent.getPointerCount();
            //getRadius  触摸半径
            float radius = touchEvent.getRadius(0) * 2 / 3;
            // 相对于屏幕的坐标
            MmiPoint screenPos = touchEvent.getPointerScreenPosition(0);
            Size size = getPixelMap() == null ? new Size(0, 0) : getPixelMap().getImageInfo().size;
            boolean isLangHeight = size.height > size.width * 3;  // 判定是否为竖长图

            switch (touchEvent.getAction()) {
                case TouchEvent.PRIMARY_POINT_DOWN:
                    pointFirst = new Point(screenPos.getX(), screenPos.getY());
                    pointLastMove = new Point(screenPos.getX(), screenPos.getY());
                    lastMoveTime = System.currentTimeMillis();
                    startPosition = getContentPositionY();
                    break;
                case TouchEvent.POINT_MOVE:
                    if (pointFirst != null) {
                        float absx = pointLastMove.getPointX() - screenPos.getX();
                        float absy = pointLastMove.getPointY() - screenPos.getY();
                        float dis = absx * absx + absy * absy;
                        if (dis > radius * radius) {
                            float x = screenPos.getX() - pointFirst.getPointX();
                            float y = screenPos.getY() - pointFirst.getPointY();
                            if (isLangHeight) {
                                setContentPosition(x, y + startPosition);
                            } else {
                                setContentPosition(x, y);
                            }
                            lastMoveTime = System.currentTimeMillis();
                            pointLastMove.modify(screenPos.getX(), screenPos.getY());
                        }
                    }
                    if (pointerCount == 2) {
                        moveDist = spacing(touchEvent);
                        double space = moveDist - oldDist;
                        float scale = (float) (getScaleX() + space / getWidth());
                        if (scale < SCALE_MIN) {
                            setScale(SCALE_MIN);
                        } else if (scale > MAX_SCALE) {
                            setScale(MAX_SCALE);
                        } else {
                            setScale(scale);
                        }
                    }
                    break;
                case TouchEvent.OTHER_POINT_DOWN:
                    if (pointerCount == 2) {
                        downX1 = touchEvent.getPointerPosition(0).getX();
                        downX2 = touchEvent.getPointerPosition(1).getX();
                        downY1 = touchEvent.getPointerPosition(0).getY();
                        downY2 = touchEvent.getPointerPosition(1).getY();
                        oldDist = spacing(touchEvent); //两点按下时的距离
                    }
                    break;
                case TouchEvent.PRIMARY_POINT_UP:
                    if (isLangHeight) {
                        this.setContentPositionX(0);
                    } else {
                        this.setPosition(0, 0);
                    }
                    pointFirst = null;
                    break;
            }
        }

        return true;
    }

    /**
     * 设置放大缩小
     *
     * @param scale
     */
    public void setScale(float scale) {
        setScaleX(scale);
        setScaleY(scale);
    }

    private float spacing(TouchEvent event) {
        if (event.getPointerCount() == 2) {
            float x = event.getPointerPosition(0).getX() - event.getPointerPosition(1).getX();
            float y = event.getPointerPosition(0).getY() - event.getPointerPosition(1).getY();
            return (float) Math.sqrt(x * x + y * y);
        } else {
            return 0;
        }
    }


    /**
     * 获取默认的动画持续时间
     */
    public int getDefaultAnimaDuring() {
        return ANIMA_DURING;
    }

    @Override
    public void setClickedListener(ClickedListener l) {
        super.setClickedListener(l);

    }

    public void setScaleType(Image.ScaleMode scaleType) {
        Image.ScaleMode old = mScaleType;
        mScaleType = scaleType;
        setScaleMode(scaleType);

        if (old != scaleType)
            initBase();
    }

    @Override
    public void setLongClickedListener(LongClickedListener listener) {
        super.setLongClickedListener(listener);
        mLongClick = listener;
    }

    /**
     * 获取动画持续时间
     */
    public int getAnimaDuring() {
        return mAnimaDuring;
    }

    /**
     * 设置动画的持续时间
     */
    public void setAnimaDuring(int during) {
        mAnimaDuring = during;
    }

    /**
     * 设置最大可以缩放的倍数
     */
    public void setMaxScale(float maxScale) {
        mMaxScale = maxScale;
    }

    /**
     * 获取最大可以缩放的倍数
     */
    public float getMaxScale() {
        return mMaxScale;
    }

    /**
     * 启用手势缩放功能
     */
    public void enableGesture() {
        isEnableGesture = true;
    }

    /**
     * 获取当前是否启用缩放功能的状态
     */
    public boolean isEnableGesture() {
        return isEnableGesture;
    }

    /**
     * 禁用缩放功能
     */
    public void disable() {
        System.out.println("禁用缩放功能 ");
        isEnableGesture = false;
    }

    /**
     *
     */
    public void setMaxAnimFromWaiteTime(int wait) {
        MAX_ANIM_FROM_WAITE = wait;
    }

    @Override
    public void setPixelMap(PixelMap pixelMap) {
        super.setPixelMap(pixelMap);

    }


    public void setImage(Image image) {
        setPixelMap(image.getPixelMap());
        setBackground(DisplayUtils.getTransparentElement());

    }

    public void setImageDrawable(Element drawable) {
        PixelMap pixelMap = ((PixelMapElement) (drawable)).getPixelMap();
        setPixelMap(pixelMap);

        if (!hasSize(drawable))
            return;

        if (!hasDrawable) {
            hasDrawable = true;
        }
        initBase();
    }

    private boolean hasSize(Element d) {
        if ((d.getHeight() <= 0 || d.getWidth() <= 0)) {
            return false;
        }
        return true;
    }

    private static int getDrawableWidth(Drawable d) {
        int width = d.getWidth();
        return width;
    }

    private static int getDrawableHeight(Drawable d) {
        int height = d.getHeight();
        return height;
    }

    private void initBase() {
        if (!hasDrawable) return;
        if (!isKnowSize) return;

        mBaseMatrix.reset();
        mAnimaMatrix.reset();

        isZoomUp = false;

        Drawable img = getDrawable();

        int w = getWidth();
        int h = getHeight();
        int imgw = getDrawableWidth(img);
        int imgh = getDrawableHeight(img);

        mBaseRect.modify(0, 0, imgw, imgh);

        // 以图片中心点居中位移
        int tx = (w - imgw) / 2;
        int ty = (h - imgh) / 2;

        float sx = 1;
        float sy = 1;

        // 缩放，默认不超过屏幕大小
        if (imgw > w) {
            sx = (float) w / imgw;
        }

        if (imgh > h) {
            sy = (float) h / imgh;
        }

        float scale = sx < sy ? sx : sy;

        mBaseMatrix.reset();
        mBaseMatrix.postTranslate(tx, ty);
        mBaseMatrix.postScale(scale, scale, mScreenCenter.getPointX(), mScreenCenter.getPointY());
        mBaseMatrix.mapRect(mBaseRect);

        mHalfBaseRectWidth = mBaseRect.getWidth() / 2;
        mHalfBaseRectHeight = mBaseRect.getHeight() / 2;

        mScaleCenter.modify(mScreenCenter);
        mRotateCenter.modify(mScaleCenter);

        executeTranslate();

        switch (mScaleType) {
            case CENTER:
                initCenter();
                break;
            case CLIP_CENTER:
                initCenterCrop();
                break;
            case INSIDE:
                initCenterInside();
                break;
            case ZOOM_CENTER:
                initFitCenter();
                break;
            case ZOOM_START:
                initFitStart();
                break;
            case ZOOM_END:
                initFitEnd();
                break;
            case STRETCH:
                initFitXY();
                break;
        }

        isInit = true;

        if (mFromInfo != null && System.currentTimeMillis() - mInfoTime < MAX_ANIM_FROM_WAITE) {
            animaFrom(mFromInfo);
        }

        mFromInfo = null;

        if (imgh > imgw * 3) { // 判定是否为竖长图
            mAnimaMatrix.postTranslate(0, -mImgRect.top);
            mTranslateY -= mImgRect.top;
            executeTranslate();
        }
    }

    public Drawable getDrawable() {
        return Drawable.create(this);
    }

    private void initCenter() {
        if (!hasDrawable) return;
        if (!isKnowSize) return;

        Drawable img = getDrawable();

        int imgw = getDrawableWidth(img);
        int imgh = getDrawableHeight(img);

        if (imgw > mWidgetRect.getWidth() || imgh > mWidgetRect.getHeight()) {
            float scaleX = imgw / mImgRect.getWidth();
            float scaleY = imgh / mImgRect.getHeight();

            mScale = scaleX > scaleY ? scaleX : scaleY;

            mAnimaMatrix.postScale(mScale, mScale, mScreenCenter.getPointX(), mScreenCenter.getPointY());

            executeTranslate();

            resetBase();
        }
    }

    private void initCenterCrop() {
        if (mImgRect.getWidth() < mWidgetRect.getWidth() || mImgRect.getHeight() < mWidgetRect.getHeight()) {
            float scaleX = mWidgetRect.getWidth() / mImgRect.getWidth();
            float scaleY = mWidgetRect.getHeight() / mImgRect.getHeight();

            mScale = scaleX > scaleY ? scaleX : scaleY;

            mAnimaMatrix.postScale(mScale, mScale, mScreenCenter.getPointX(), mScreenCenter.getPointY());

            executeTranslate();
            resetBase();
        }
    }

    private void initCenterInside() {
        if (mImgRect.getWidth() > mWidgetRect.getWidth() || mImgRect.getHeight() > mWidgetRect.getHeight()) {
            float scaleX = mWidgetRect.getWidth() / mImgRect.getWidth();
            float scaleY = mWidgetRect.getHeight() / mImgRect.getHeight();

            mScale = scaleX < scaleY ? scaleX : scaleY;

            mAnimaMatrix.postScale(mScale, mScale, mScreenCenter.getPointX(), mScreenCenter.getPointY());

            executeTranslate();
            resetBase();
        }
    }

    private void initFitCenter() {
        if (mImgRect.getWidth() < mWidgetRect.getWidth()) {
            mScale = mWidgetRect.getWidth() / mImgRect.getWidth();
            mAnimaMatrix.postScale(mScale, mScale, mScreenCenter.getPointX(), mScreenCenter.getPointY());
            executeTranslate();
            resetBase();
        }
    }

    private void initFitStart() {
        initFitCenter();

        float ty = -mImgRect.top;
        mTranslateY += ty;
        mAnimaMatrix.postTranslate(0, ty);
        executeTranslate();
        resetBase();
    }

    private void initFitEnd() {
        initFitCenter();

        float ty = (mWidgetRect.bottom - mImgRect.bottom);
        mTranslateY += ty;
        mAnimaMatrix.postTranslate(0, ty);
        executeTranslate();
        resetBase();
    }

    private void initFitXY() {
        float scaleX = mWidgetRect.getWidth() / mImgRect.getWidth();
        float scaleY = mWidgetRect.getHeight() / mImgRect.getHeight();

        mAnimaMatrix.postScale(scaleX, scaleY, mScreenCenter.getPointX(), mScreenCenter.getPointY());

        executeTranslate();
        resetBase();
    }

    private void resetBase() {
        Drawable img = getDrawable();
        int imgw = getDrawableWidth(img);
        int imgh = getDrawableHeight(img);
        mBaseRect.modify(0, 0, imgw, imgh);
        mBaseMatrix.setMatrix(mSynthesisMatrix);
        mBaseMatrix.mapRect(mBaseRect);
        mHalfBaseRectWidth = mBaseRect.getWidth() / 2;
        mHalfBaseRectHeight = mBaseRect.getHeight() / 2;
        mScale = 1;
        mTranslateX = 0;
        mTranslateY = 0;
        mAnimaMatrix.reset();
    }

    private void executeTranslate() {
        mSynthesisMatrix.setMatrix(mBaseMatrix);
        mSynthesisMatrix.postConcat(mAnimaMatrix);
        Canvas canvas = new Canvas(new Texture(getPixelMap()));
        Paint paint = new Paint();
        canvas.setMatrix(mSynthesisMatrix);
        canvas.drawPixelMapHolder(new PixelMapHolder(getPixelMap()), 0, 0, paint);

        mAnimaMatrix.mapRect(mImgRect, mBaseRect);

        imgLargeWidth = mImgRect.getWidth() > mWidgetRect.getWidth();
        imgLargeHeight = mImgRect.getHeight() > mWidgetRect.getHeight();
    }

    @Override
    public void estimateSize(int widthEstimatedConfig, int heightEstimatedConfig) {
        super.estimateSize(widthEstimatedConfig, heightEstimatedConfig);
        onMeasure(widthEstimatedConfig, heightEstimatedConfig);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (!hasDrawable) {
//            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }

        Drawable d = getDrawable();//getDrawable();
        int drawableW = getDrawableWidth(d);
        int drawableH = getDrawableHeight(d);

        int pWidth = Component.MeasureSpec.getSize(widthMeasureSpec);
        int pHeight = Component.MeasureSpec.getSize(heightMeasureSpec);

        int widthMode = Component.MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = Component.MeasureSpec.getMode(heightMeasureSpec);

        int width = 0;
        int height = 0;

        ComponentContainer.LayoutConfig p = getLayoutConfig();
        if (p == null) {
            p = new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        }

        if (p.width == ComponentContainer.LayoutConfig.MATCH_PARENT) {
            if (widthMode == EstimateSpec.UNCONSTRAINT) {
                width = drawableW;
            } else {
                width = pWidth;
            }
        } else {
            if (widthMode == EstimateSpec.NOT_EXCEED) {
                width = pWidth;
            } else {
                width = drawableW;
            }
        }

        if (p.height == ComponentContainer.LayoutConfig.MATCH_PARENT) {
            if (heightMode == EstimateSpec.UNCONSTRAINT) {
                height = drawableH;
            } else {
                height = pHeight;
            }
        } else {
            if (heightMode == EstimateSpec.NOT_EXCEED) {
                height = pHeight;
            } else {
                height = drawableH;
            }
        }

        float drawB = (float) drawableW / drawableH;
        float wh = (float) width / height;
        int compare = new BigDecimal(String.valueOf(drawB)).compareTo(new BigDecimal(String.valueOf(wh)));
        if (mAdjustViewBounds && compare != 0) {

            float hScale = (float) height / drawableH;
            float wScale = (float) width / drawableW;

            float scale = new BigDecimal(String.valueOf(hScale)).min(new BigDecimal(String.valueOf(wScale))).floatValue();
            width = p.width == ComponentContainer.LayoutConfig.MATCH_PARENT ? width : (int) (drawableW * scale);
            height = p.height == ComponentContainer.LayoutConfig.MATCH_PARENT ? height : (int) (drawableH * scale);
        }

        setEstimatedSize(width, height);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (mClip != null) {
            canvas.clipRect(mClip);
            mClip = null;
        }
    }

    private void onUp(TouchEvent event) {
        if (canRotate || mDegrees % 90 != 0) {
            float toDegrees = (int) (mDegrees / 90) * 90;
            float remainder = mDegrees % 90;

            if (remainder > 45)
                toDegrees += 90;
            else if (remainder < -45)
                toDegrees -= 90;

            mDegrees = toDegrees;
        }

        float scale = mScale;

        if (mScale < 1) {
            scale = 1;
        } else if (mScale > mMaxScale) {
            scale = mMaxScale;
        }

        float cx = mImgRect.left + mImgRect.getWidth() / 2;
        float cy = mImgRect.top + mImgRect.getHeight() / 2;

        mScaleCenter.modify(cx, cy);
        mRotateCenter.modify(cx, cy);

        mTranslateX = 0;
        mTranslateY = 0;

        mTmpMatrix.reset();
        mTmpMatrix.postTranslate(-mBaseRect.left, -mBaseRect.top);
        mTmpMatrix.postTranslate(cx - mHalfBaseRectWidth, cy - mHalfBaseRectHeight);
        mTmpMatrix.postScale(scale, scale, cx, cy);
        mTmpMatrix.postRotate(mDegrees, cx, cy);
        mTmpMatrix.mapRect(mTmpRect, mBaseRect);

        doTranslateReset(mTmpRect);
    }

    private void doTranslateReset(RectFloat imgRect) {
        int tx = 0;
        int ty = 0;

        if (imgRect.getWidth() <= mWidgetRect.getWidth()) {
            if (!isImageCenterWidth(imgRect))
                tx = -(int) ((mWidgetRect.getWidth() - imgRect.getWidth()) / 2 - imgRect.left);
        } else {
            if (imgRect.left > mWidgetRect.left) {
                tx = (int) (imgRect.left - mWidgetRect.left);
            } else if (imgRect.right < mWidgetRect.right) {
                tx = (int) (imgRect.right - mWidgetRect.right);
            }
        }

        if (imgRect.getHeight() <= mWidgetRect.getHeight()) {
            if (!isImageCenterHeight(imgRect))
                ty = -(int) ((mWidgetRect.getHeight() - imgRect.getHeight()) / 2 - imgRect.top);
        } else {
            if (imgRect.top > mWidgetRect.top) {
                ty = (int) (imgRect.top - mWidgetRect.top);
            } else if (imgRect.bottom < mWidgetRect.bottom) {
                ty = (int) (imgRect.bottom - mWidgetRect.bottom);
            }
        }
    }

    private boolean isImageCenterHeight(RectFloat rect) {
        return Math.abs(Math.round(rect.top) - (mWidgetRect.getHeight() - rect.getHeight()) / 2) < 1;
    }

    private boolean isImageCenterWidth(RectFloat rect) {
        return Math.abs(Math.round(rect.left) - (mWidgetRect.getWidth() - rect.getWidth()) / 2) < 1;
    }

    private RotateGestureDetector.OnRotateListener mRotateListener = new RotateGestureDetector.OnRotateListener() {

        @Override
        public void onRotate(float degrees, float focusX, float focusY) {
            mRotateFlag += degrees;
            if (canRotate) {
                mDegrees += degrees;
                mAnimaMatrix.postRotate(degrees, focusX, focusY);
            } else {
                if (Math.abs(mRotateFlag) >= mMinRotate) {
                    canRotate = true;
                    mRotateFlag = 0;
                }
            }
        }
    };

    private float resistanceScrollByX(float overScroll, float detalX) {
        float s = detalX * (Math.abs(Math.abs(overScroll) - MAX_OVER_RESISTANCE) / (float) MAX_OVER_RESISTANCE);
        return s;
    }

    private float resistanceScrollByY(float overScroll, float detalY) {
        float s = detalY * (Math.abs(Math.abs(overScroll) - MAX_OVER_RESISTANCE) / (float) MAX_OVER_RESISTANCE);
        return s;
    }

    /**
     * 匹配两个Rect的共同部分输出到out，若无共同部分则输出0，0，0，0
     */
    private void mapRect(RectFloat r1, RectFloat r2, RectFloat out) {

        float l, r, t, b;

        l = r1.left > r2.left ? r1.left : r2.left;
        r = r1.right < r2.right ? r1.right : r2.right;

        if (l > r) {
            out.modify(0, 0, 0, 0);
            return;
        }

        t = r1.top > r2.top ? r1.top : r2.top;
        b = r1.bottom < r2.bottom ? r1.bottom : r2.bottom;

        if (t > b) {
            out.modify(0, 0, 0, 0);
            return;
        }

        out.modify(l, t, r, b);
    }

    private void checkRect() {
        if (!hasOverTranslate) {
            mapRect(mWidgetRect, mImgRect, mCommonRect);
        }
    }

    private Runnable mClickRunnable = new Runnable() {
        @Override
        public void run() {
            if (mClickListener != null) {
            }
        }
    };


    EventHandler handler = new EventHandler(EventRunner.getMainEventRunner());
    //手势监听
    private GestureDetector.OnGestureListener mGestureListener = new GestureDetector.SimpleOnGestureListener() {
        @Override
        public boolean onDown(TouchEvent e) {
            hasOverTranslate = false;
            hasMultiTouch = false;
            canRotate = false;
            handler.postTask(mClickRunnable);
            return true;
        }

        @Override
        public boolean onFling(TouchEvent e1, TouchEvent e2, float velocityX, float velocityY) {
            if (hasMultiTouch) return false;
            if (!imgLargeWidth && !imgLargeHeight) return false;

            float vx = velocityX;
            float vy = velocityY;

            if (Math.round(mImgRect.left) >= mWidgetRect.left || Math.round(mImgRect.right) <= mWidgetRect.right) {
                vx = 0;
            }

            if (Math.round(mImgRect.top) >= mWidgetRect.top || Math.round(mImgRect.bottom) <= mWidgetRect.bottom) {
                vy = 0;
            }

            if (canRotate || mDegrees % 90 != 0) {
                float toDegrees = (int) (mDegrees / 90) * 90;
                float remainder = mDegrees % 90;

                if (remainder > 45)
                    toDegrees += 90;
                else if (remainder < -45)
                    toDegrees -= 90;


                mDegrees = toDegrees;

            }

            doTranslateReset(mImgRect);

            onUp(e2);
            return super.onFling(e1, e2, velocityX, velocityY);
        }

        @Override
        public boolean onScroll(TouchEvent e1, TouchEvent e2, float distanceX, float distanceY) {
            //设置旋转
            if (isEnableGesture) {
                mPhotoView.setRotation(mRotateFlag);
            }

            if (canScrollHorizontallySelf(distanceX)) {
                if (distanceX < 0 && mImgRect.left - distanceX > mWidgetRect.left)
                    distanceX = mImgRect.left;
                if (distanceX > 0 && mImgRect.right - distanceX < mWidgetRect.right)
                    distanceX = mImgRect.right - mWidgetRect.right;

                mAnimaMatrix.postTranslate(-distanceX, 0);
                mTranslateX -= distanceX;
            } else if (imgLargeWidth || hasMultiTouch || hasOverTranslate) {
                checkRect();
                if (!hasMultiTouch) {
                    if (distanceX < 0 && mImgRect.left - distanceX > mCommonRect.left)
                        distanceX = resistanceScrollByX(mImgRect.left - mCommonRect.left, distanceX);
                    if (distanceX > 0 && mImgRect.right - distanceX < mCommonRect.right)
                        distanceX = resistanceScrollByX(mImgRect.right - mCommonRect.right, distanceX);
                }

                mTranslateX -= distanceX;
                mAnimaMatrix.postTranslate(-distanceX, 0);
                hasOverTranslate = true;
            }

            if (canScrollVerticallySelf(distanceY)) {
                if (distanceY < 0 && mImgRect.top - distanceY > mWidgetRect.top)
                    distanceY = mImgRect.top;
                if (distanceY > 0 && mImgRect.bottom - distanceY < mWidgetRect.bottom)
                    distanceY = mImgRect.bottom - mWidgetRect.bottom;

                mAnimaMatrix.postTranslate(0, -distanceY);
                mTranslateY -= distanceY;
            } else if (imgLargeHeight || hasOverTranslate || hasMultiTouch) {
                checkRect();
                if (!hasMultiTouch) {
                    if (distanceY < 0 && mImgRect.top - distanceY > mCommonRect.top)
                        distanceY = resistanceScrollByY(mImgRect.top - mCommonRect.top, distanceY);
                    if (distanceY > 0 && mImgRect.bottom - distanceY < mCommonRect.bottom)
                        distanceY = resistanceScrollByY(mImgRect.bottom - mCommonRect.bottom, distanceY);
                }

                mAnimaMatrix.postTranslate(0, -distanceY);
                mTranslateY -= distanceY;
                hasOverTranslate = true;
            }

            return true;
        }

        @Override
        public boolean onSingleTapUp(TouchEvent e) {
            handler.postTask(mClickRunnable, 250);
            return super.onSingleTapUp(e);
        }
    };


    public boolean canScrollHorizontallySelf(float direction) {
        if (mImgRect.getWidth() <= mWidgetRect.getWidth()) return false;
        if (direction < 0 && Math.round(mImgRect.left) - direction >= mWidgetRect.left)
            return false;
        if (direction > 0 && Math.round(mImgRect.right) - direction <= mWidgetRect.right)
            return false;
        return true;
    }

    public boolean canScrollVerticallySelf(float direction) {
        if (mImgRect.getHeight() <= mWidgetRect.getHeight()) return false;
        if (direction < 0 && Math.round(mImgRect.top) - direction >= mWidgetRect.top)
            return false;
        if (direction > 0 && Math.round(mImgRect.bottom) - direction <= mWidgetRect.bottom)
            return false;
        return true;
    }


    public ImageInfo getInfo() {
        RectFloat rect = new RectFloat();
        int[] p = new int[2];
        getLocation(this, p);
        rect.modify(p[0] + mImgRect.left, p[1] + mImgRect.top, p[0] + mImgRect.right, p[1] + mImgRect.bottom);
        return new ImageInfo(rect, mImgRect, mWidgetRect, mBaseRect, mScale, mDegrees, mScaleType);
    }

    public static ImageInfo getImageViewInfo(Image imgView) {
        int[] p = new int[2];
        getLocation(imgView, p);

        Drawable drawable = Drawable.create(imgView);//imgView.getDrawable();

        int width = getDrawableWidth(drawable);
        int height = getDrawableHeight(drawable);

        RectFloat imgRect = new RectFloat(0, 0, width, height);

        RectFloat rect = new RectFloat(p[0] + imgRect.left, p[1] + imgRect.top, p[0] + imgRect.right, p[1] + imgRect.bottom);
        RectFloat widgetRect = new RectFloat(0, 0, imgView.getWidth(), imgView.getHeight());
        RectFloat baseRect = new RectFloat(widgetRect);

        return new ImageInfo(rect, imgRect, widgetRect, baseRect, 1, 0, imgView.getScaleMode());
    }

    private static void getLocation(Component target, int[] position) {

        position[0] += target.getLeft();
        position[1] += target.getTop();

        ComponentParent viewParent = target.getComponentParent();
        while (viewParent instanceof Component) {
            final Component view = (Component) viewParent;

            position[0] -= view.getScrollValue(Component.AXIS_X);
            position[1] -= view.getScrollValue(Component.AXIS_Y);

            position[0] += view.getLeft();
            position[1] += view.getTop();

            viewParent = view.getComponentParent();
        }

        position[0] = (int) (position[0] + 0.5f);
        position[1] = (int) (position[1] + 0.5f);
    }

    public boolean isPhotoNotChanged() {
        return mDegrees == 0.f && mScale == 1.f && getTranslationX() == 0.f && getTranslationY() == 0.f;
    }

    public boolean isScrollTop() {
        return mImgRect.top >= 0;
    }

    public void reset() {
        mAnimaMatrix.reset();
        executeTranslate();
        mScale = 1;
        mTranslateX = 0;
        mTranslateY = 0;
    }

    @Override
    public void onDoubleClick(Component component) {
        if (!isEnableGesture)return;
        isDouble = true;
        float imgcx = getWidth() / 2;
        float imgcy = getHeight() / 2;

        mScaleCenter.translate(imgcx, imgcy);
        mRotateCenter.translate(imgcx, imgcy);
        mTranslateX = 0;
        mTranslateY = 0;
        if (isZoomUp) {
            setScale(SCALE_MIN);
        } else {
            setScale(MAX_SCALE);
        }

        isZoomUp = !isZoomUp;

    }


    public interface ClipCalculate {
        float calculateTop();
    }

    public class START implements ClipCalculate {
        public float calculateTop() {
            return mImgRect.top;
        }
    }

    public class END implements ClipCalculate {
        public float calculateTop() {
            return mImgRect.bottom;
        }
    }

    public class OTHER implements ClipCalculate {
        public float calculateTop() {
            return (mImgRect.top + mImgRect.bottom) / 2;
        }
    }

    /**
     * 在PhotoView内部还没有图片的时候同样可以调用该方法
     * <p></p>
     * 此时并不会播放动画，当给PhotoView设置图片后会自动播放动画。
     * <p></p>
     * 若等待时间过长也没有给控件设置图片，则会忽略该动画，若要再次播放动画则需要重新调用该方法
     * (等待的时间默认500毫秒，可以通过setMaxAnimFromWaiteTime(int)设置最大等待时间)
     */
    public void animaFrom(ImageInfo info) {
        if (isInit) {
            reset();

            ImageInfo mine = getInfo();

            float scaleX = info.mImgRect.getWidth() / mine.mImgRect.getWidth();
            float scaleY = info.mImgRect.getHeight() / mine.mImgRect.getHeight();
            float scale = scaleX < scaleY ? scaleX : scaleY;

            float ocx = info.mRect.left + info.mRect.getWidth() / 2;
            float ocy = info.mRect.top + info.mRect.getHeight() / 2;

            mAnimaMatrix.reset();
            mAnimaMatrix.postTranslate(-mBaseRect.left, -mBaseRect.top);
            mAnimaMatrix.postTranslate(ocx - mBaseRect.getWidth() / 2, ocy - mBaseRect.getHeight() / 2);
            mAnimaMatrix.postScale(scale, scale, ocx, ocy);
            mAnimaMatrix.postRotate(info.mDegrees, ocx, ocy);
            executeTranslate();

            mScaleCenter.modify(ocx, ocy);
            mRotateCenter.modify(ocx, ocy);

            if (info.mWidgetRect.getWidth() < info.mImgRect.getWidth() || info.mWidgetRect.getHeight() < info.mImgRect.getHeight()) {
                float clipX = info.mWidgetRect.getWidth() / info.mImgRect.getWidth();
                float clipY = info.mWidgetRect.getHeight() / info.mImgRect.getHeight();
                clipX = clipX > 1 ? 1 : clipX;
                clipY = clipY > 1 ? 1 : clipY;

                ClipCalculate c = info.mScaleType == Image.ScaleMode.ZOOM_START ? new START() : info.mScaleType == Image.ScaleMode.ZOOM_END ? new END() : new OTHER();

                mTmpMatrix.setScale(clipX, clipY, (mImgRect.left + mImgRect.right) / 2, c.calculateTop());
            }
        } else {
            mFromInfo = info;
            mInfoTime = System.currentTimeMillis();
        }
    }

    public void animaTo(ImageInfo info, Runnable completeCallBack) {
        if (isInit) {

            mTranslateX = 0;
            mTranslateY = 0;

            float tcx = info.mRect.left + info.mRect.getWidth() / 2;
            float tcy = info.mRect.top + info.mRect.getHeight() / 2;

            mScaleCenter.modify(mImgRect.left + mImgRect.getWidth() / 2, mImgRect.top + mImgRect.getHeight() / 2);
            mRotateCenter.modify(mScaleCenter);

            // 将图片旋转回正常位置，用以计算
            mAnimaMatrix.postRotate(-mDegrees, mScaleCenter.getPointX(), mScaleCenter.getPointY());
            mAnimaMatrix.mapRect(mImgRect, mBaseRect);

            // 缩放
            float scaleX = info.mImgRect.getWidth() / mBaseRect.getWidth();
            float scaleY = info.mImgRect.getHeight() / mBaseRect.getHeight();
            float scale = scaleX > scaleY ? scaleX : scaleY;

            mAnimaMatrix.postRotate(mDegrees, mScaleCenter.getPointX(), mScaleCenter.getPointY());
            mAnimaMatrix.mapRect(mImgRect, mBaseRect);

            mDegrees = mDegrees % 360;

            if (info.mWidgetRect.getWidth() < info.mRect.getWidth() || info.mWidgetRect.getHeight() < info.mRect.getHeight()) {
                float clipX = info.mWidgetRect.getWidth() / info.mRect.getWidth();
                float clipY = info.mWidgetRect.getHeight() / info.mRect.getHeight();
                clipX = clipX > 1 ? 1 : clipX;
                clipY = clipY > 1 ? 1 : clipY;

                final float cx = clipX;
                final float cy = clipY;
                final ClipCalculate c = info.mScaleType == Image.ScaleMode.ZOOM_START ? new START() : info.mScaleType == Image.ScaleMode.ZOOM_END ? new END() : new OTHER();

            }

            mCompleteCallBack = completeCallBack;
        }
    }
}