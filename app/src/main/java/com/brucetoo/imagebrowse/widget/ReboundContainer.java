package com.brucetoo.imagebrowse.widget;

import com.brucetoo.imagebrowse.ComponentConfiguration;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.multimodalinput.event.TouchEvent;

 public abstract class ReboundContainer<T extends Component> extends DependentLayout implements Component.TouchEventListener {

    private T mOverscrollView = null;

    private boolean mIsBeingDragged = false;

    private float mMotionBeginX = 0;

    private float mMotionBeginY = 0;

    private int mTouchSlop;

    public enum OverscrollDirection {
        Horizontal, Vertical,
    }

    abstract protected boolean canOverscrollAtStart();

    abstract protected boolean canOverscrollAtEnd();

    abstract protected OverscrollDirection getOverscrollDirection();

    abstract protected T createOverscrollView();

    public ReboundContainer(Context context) {
        this(context, null);
    }

    public ReboundContainer(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    public ReboundContainer(Context context, AttrSet attrs, int defStyle) {
        super(context, attrs);

        mOverscrollView = createOverscrollView();
        LayoutConfig layoutParams = new LayoutConfig(
                LayoutConfig.MATCH_PARENT,
                LayoutConfig.MATCH_PARENT);
        addComponent(mOverscrollView, layoutParams);

        mTouchSlop = ComponentConfiguration.getScaledTouchSlop();
    }

    public T getOverscrollView() {
        return mOverscrollView;
    }



    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int action = touchEvent.getAction();

        float moveOffset = 0;
        if (getOverscrollDirection() == OverscrollDirection.Horizontal) {

            moveOffset = touchEvent.getPointerScreenPosition(touchEvent.getIndex()).getX() - mMotionBeginX;

        } else if (getOverscrollDirection() == OverscrollDirection.Vertical) {

            moveOffset = touchEvent.getPointerScreenPosition(touchEvent.getIndex()).getY() - mMotionBeginY;

        }

        moveOffset *= 0.5f;

        if (action == TouchEvent.POINT_MOVE) {

            if (getOverscrollDirection() == OverscrollDirection.Horizontal) {
                moveOverscrollView(moveOffset, 0);
            } else if (getOverscrollDirection() == OverscrollDirection.Vertical) {
                moveOverscrollView(0, moveOffset);
            }



        } else if (action == TouchEvent.OTHER_POINT_UP) {

            resetOverscrollViewWithAnimation(moveOffset, touchEvent.getPointerScreenPosition(touchEvent.getIndex()).getY());
            mIsBeingDragged = false;

        }

        return true;
    }


    private void moveOverscrollView(float currentX, float currentY) {
        if (getOverscrollDirection() == OverscrollDirection.Horizontal) {

            scrollTo(-(int) currentX, 0);

        } else if (getOverscrollDirection() == OverscrollDirection.Vertical) {

            scrollTo(0, -(int) currentY);

        }
    }

    private void resetOverscrollViewWithAnimation(float currentX, float currentY) {
        SmoothScrollRunnable smoothScrollRunnable = new SmoothScrollRunnable((int) currentX, 0, 300);
        EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());
        eventHandler.postTask(smoothScrollRunnable);
    }

    final class SmoothScrollRunnable implements Runnable {
        private final int mScrollToPosition;
        private final int mScrollFromPosition;
        private final long mDuration;

        private boolean mContinueRunning = true;
        private long mStartTime = -1;
        private int mCurrentPosition = -1;

        public SmoothScrollRunnable(int fromPosition, int toPosition, long duration) {
            mScrollFromPosition = fromPosition;
            mScrollToPosition = toPosition;
            mDuration = duration;
        }

        @Override
        public void run() {

            /**
             * Only set mStartTime if this is the first time we're starting,
             * else actually calculate the Y delta
             */
            if (mStartTime == -1) {
                mStartTime = System.currentTimeMillis();
            } else {

                /**
                 * We do do all calculations in long to reduce software float
                 * calculations. We use 1000 as it gives us good accuracy and
                 * small rounding errors
                 */
                long normalizedTime = (1000 * (System.currentTimeMillis() - mStartTime)) / mDuration;
                normalizedTime = Math.max(Math.min(normalizedTime, 1000), 0);

                final int deltaY = Math.round((mScrollFromPosition - mScrollToPosition));
                mCurrentPosition = mScrollFromPosition - deltaY;

                if (getOverscrollDirection() == OverscrollDirection.Horizontal) {
                    moveOverscrollView(mCurrentPosition, 0);
                } else if (getOverscrollDirection() == OverscrollDirection.Vertical) {
                    moveOverscrollView(0, mCurrentPosition);
                }

            }
        }

        public void stop() {
            mContinueRunning = false;
        }
    }

}
