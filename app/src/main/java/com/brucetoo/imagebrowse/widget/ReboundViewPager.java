package com.brucetoo.imagebrowse.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.PageSlider;
import ohos.agp.components.PageSliderProvider;
import ohos.app.Context;

/**
 * Created by Bruce Too
 * On 9/29/15.
 * At 20:57
 * PageSlider with rebound effect
 * like Chris banes's pulltorefreshview
 */
public class ReboundViewPager extends ReboundContainer<PageSlider> {

    public ReboundViewPager(Context context) {
        this(context, null);
    }

    public ReboundViewPager(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    public ReboundViewPager(Context context, AttrSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected boolean canOverscrollAtStart() {
        PageSlider viewPager = getOverscrollView();
        PageSliderProvider adapter = viewPager.getProvider();
        if (null != adapter) {
            if (viewPager.getCurrentPage() == 0) {
                return true;
            }
            return false;
        }
        return false;
    }

    @Override
    protected boolean canOverscrollAtEnd() {
        PageSlider viewPager = getOverscrollView();
        PageSliderProvider adapter = viewPager.getProvider();
        if (null != adapter && adapter.getCount() > 0) {
            if (viewPager.getCurrentPage() == adapter.getCount() - 1) {
                return true;
            }
            return false;
        }

        return false;
    }

    @Override
    protected OverscrollDirection getOverscrollDirection() {
        return OverscrollDirection.Horizontal;
    }

    @Override
    protected PageSlider createOverscrollView() {
        PageSlider viewPager = new PageSlider(getContext());
        return viewPager;
    }

}