package com.brucetoo.imagebrowse.widget;

import ohos.multimodalinput.event.TouchEvent;

/**
 * Created by Bruce too
 * On 2016/4/18
 * At 20:27
 */
public class RotateGestureDetector {

    private static final int MAX_DEGREES_STEP = 120;

    private OnRotateListener mListener;

    private float mPrevSlope;
    private float mCurrSlope;

    private float x1;
    private float y1;
    private float x2;
    private float y2;

    public RotateGestureDetector(OnRotateListener l) {
        mListener = l;
    }

    public void onTouchEvent(TouchEvent event) {

        final int Action = event.getAction();

        switch (Action) {
            case TouchEvent.PRIMARY_POINT_DOWN:
            case TouchEvent.PRIMARY_POINT_UP:
                if (event.getPointerCount() == 2) mPrevSlope = calculateSlope(event);
                break;
            case TouchEvent.POINT_MOVE:
                if (event.getPointerCount() > 1) {
                    mCurrSlope = calculateSlope(event);

                    double currDegrees = Math.toDegrees(Math.atan(mCurrSlope));
                    double prevDegrees = Math.toDegrees(Math.atan(mPrevSlope));

                    double deltaSlope = currDegrees - prevDegrees;

                    if (Math.abs(deltaSlope) <= MAX_DEGREES_STEP) {
                        if (mListener != null)
                            mListener.onRotate((float) deltaSlope, (x2 + x1) / 2, (y2 + y1) / 2);
                    }
                    mPrevSlope = mCurrSlope;
                }
                break;
            default:
                break;
        }
    }

    private float calculateSlope(TouchEvent event) {
        x1 = event.getPointerPosition(0).getX();
        y1 = event.getPointerPosition(0).getY();
        x2 = event.getPointerPosition(1).getX();
        y2 = event.getPointerPosition(1).getY();
        return (y2 - y1) / (x2 - x1);
    }

   public interface OnRotateListener {
        void onRotate(float degrees, float focusX, float focusY);
    }
}

