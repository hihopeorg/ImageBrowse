/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.brucetoo.imagebrowse.app;

import com.brucetoo.imagebrowse.EventHelper;
import com.brucetoo.imagebrowse.ImageBrowseFragment;
import com.brucetoo.imagebrowse.MainAbility;
import com.brucetoo.imagebrowse.ResourceTable;
import com.brucetoo.imagebrowse.widget.PhotoView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.miscservices.timeutility.Time;
import ohos.multimodalinput.event.TouchEvent;
import ohos.utils.net.Uri;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.brucetoo.imagebrowse.EventHelper.sAbilityDelegator;
import static com.brucetoo.imagebrowse.ImageBrowseFragment.RootBrowser;
import static org.junit.Assert.*;
//请注意 保持网络畅通 否则会无法进行UI测试
public class MainAbilityTest {
    private Ability mAbility;
    private Context mContext;

    @Before
    public void setUp() throws Exception {
        mAbility = EventHelper.startAbility(MainAbility.class);
        mContext = mAbility.getContext();
        Thread.sleep(3000);
    }

    @After
    public void tearDown() throws Exception {
        Thread.sleep(3000);
        EventHelper.clearAbilities();
    }

    @Test
    public void testUIItem() throws Exception {
        //点击事件
        Thread.sleep(6000);
        ListContainer listContainer = (ListContainer) mAbility.findComponentById(ResourceTable.Id_gridview);
        PhotoView component = (PhotoView)listContainer.getComponentAt(1);
        Assert.assertTrue(component.getPixelMap()!=null);
    }

    @Test
    public void testUISlide() throws Exception {
        Thread.sleep(6000);
        simulateClickOnScreen(838,967);
        Thread.sleep(3000);
        EventHelper.inputSwipe(mAbility, 100, 400, 700, 400, 1000);
        Thread.sleep(3000);
        PageSlider pageSlider = (PageSlider) RootBrowser.findComponentById(ResourceTable.Id_viewpager);
        Assert.assertTrue(pageSlider.getCurrentPage() == 1);

    }

    @Test
    public void testUIPhotoViewQuit() throws Exception {
        Thread.sleep(3000);
        simulateClickOnScreen(487,969);
        Thread.sleep(3000);
        simulateClickOnScreen(561, 1367);
        Thread.sleep(2000);
        Assert.assertTrue(ImageBrowseFragment.photoview.getBackgroundElement() == null);
    }

    @Test
    public void testUIOtherQuitPhoto() throws Exception {
        Thread.sleep(3000);
        simulateClickOnScreen(487,969);
        Thread.sleep(3000);
        simulateClickOnScreen(548, 658);
        Thread.sleep(2000);
        Assert.assertTrue(ImageBrowseFragment.photoview.getBackgroundElement() == null);
    }


    @Test
    public void testUIDownImage() throws Exception {
        Image image = new Image(mContext);
        image.setLayoutConfig(new ComponentContainer.LayoutConfig(100, 100));
        StackLayout RootView = (StackLayout) mAbility.findComponentById(ResourceTable.Id_layout_root);
        mAbility.getUITaskDispatcher().syncDispatch(new Runnable() {
            @Override
            public void run() {
                RootView.addComponent(image);
            }
        });
        Thread.sleep(3000);

        mAbility.getUITaskDispatcher().syncDispatch(new Runnable() {
            @Override
            public void run() {
                Glide.with(mContext).load(Uri.parse("https://img-pre.ivsky.com/img/tupian/pre/202103/23/pocket_watch.jpg"))
                        .addListener(new RequestListener<Element>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Element> target, boolean b) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Element element, Object o, Target<Element> target, DataSource dataSource, boolean b) {
                                image.setBackground(element);
                                return true;
                            }
                        }).into(image);
            }
        });
        Thread.sleep(3000);
        Assert.assertNotNull(image.getBackgroundElement());
    }


    private void simulateClickOnScreen(int x, int y) {
        long downTime = Time.getRealActiveTime();
        long upTime = downTime;
        TouchEvent ev1 = EventHelper.obtainTouchEvent(downTime, downTime, EventHelper.ACTION_DOWN, x, y);
        sAbilityDelegator.triggerTouchEvent(mAbility, ev1);
        TouchEvent ev2 = EventHelper.obtainTouchEvent(downTime, upTime, EventHelper.ACTION_UP, x, y);
        sAbilityDelegator.triggerTouchEvent(mAbility, ev2);
    }
}