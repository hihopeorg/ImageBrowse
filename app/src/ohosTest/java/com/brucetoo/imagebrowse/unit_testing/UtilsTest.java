/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.brucetoo.imagebrowse.unit_testing;

import com.brucetoo.imagebrowse.EventHelper;
import com.brucetoo.imagebrowse.MainAbility;
import com.brucetoo.imagebrowse.ResourceTable;
import com.brucetoo.imagebrowse.Utils;
import com.brucetoo.imagebrowse.widget.PhotoView;
import ohos.aafwk.ability.Ability;
import ohos.agp.colors.HslColor;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import org.junit.Assert;
import org.junit.Test;

public class UtilsTest {
    int[] RGBColor = {49, 106, 101};
    HslColor mHslColor = null;

    @Test
    public void testRgbToHsl() throws Exception {
        float[] rgbToHsl = Utils.rgbToHsl(RGBColor[0], RGBColor[1], RGBColor[2]);
        HslColor hslColor = new HslColor(rgbToHsl[0], rgbToHsl[1], rgbToHsl[2]);
        mHslColor = hslColor;
        Assert.assertNotNull(hslColor);
    }

    @Test
    public void testHslToRgb() throws Exception {
        float[] rgbToHsl = Utils.rgbToHsl(RGBColor[0], RGBColor[1], RGBColor[2]);
        mHslColor = new HslColor(rgbToHsl[0], rgbToHsl[1], rgbToHsl[2]);
        int[] hslToRgb = Utils.hslToRgb(mHslColor.getHue(), mHslColor.getSaturation(), mHslColor.getLightness());
        Assert.assertEquals(hslToRgb[0], RGBColor[0]);
    }

    @Test
    public void testDoubleView() throws Exception {
        Ability mAbility = EventHelper.startAbility(MainAbility.class);
        Context mContext = mAbility.getContext();
        Thread.sleep(2000);
        PhotoView photoView = new PhotoView(mContext);
        photoView.setLayoutConfig(new ComponentContainer.LayoutConfig(100, 100));
        StackLayout stackLayout = (StackLayout)mAbility.findComponentById(ResourceTable.Id_layout_root);
        Thread.sleep(2000);
        mContext.getUITaskDispatcher().delayDispatch(new Runnable() {
            @Override
            public void run() {
                stackLayout.addComponent(photoView);
            }
        },100);
        Resource resource = mAbility.getResourceManager().getResource(ResourceTable.Media_icon);
        PixelMap pixelmap = ImageSource.create(resource, null).createPixelmap(null);
        photoView.setBackground(new PixelMapElement(pixelmap));
        Thread.sleep(2000);
        EventHelper.triggerDoubleClickEvent(mAbility,photoView);
        Thread.sleep(2000);
        Assert.assertTrue(photoView.getScaleY()==2.5);
    }

}